GLOBAL cpuVendor
GLOBAL spinLock
GLOBAL spinUnlock

GLOBAL yield
section .text
	
cpuVendor:
	push rbp
	mov rbp, rsp

	push rbx

	mov rax, 0
	cpuid


	mov [rdi], ebx
	mov [rdi + 4], edx
	mov [rdi + 8], ecx

	mov byte [rdi+13], 0

	mov rax, rdi

	pop rbx

	mov rsp, rbp
	pop rbp
	ret
spinLock:
	mov rax, 1
	xchg rax, [rdi]

	test rax, rax
	jnz spinLock

	ret

spinUnlock:
	xor rax, rax
	xchg rax, [rdi]

	ret


yield:
	int 0x20
ret