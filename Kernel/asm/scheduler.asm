GLOBAL _configNewStack
GLOBAL _changeProcess

section .text

_configNewStack:
    push rbp
	mov rbp, rsp

    mov  rsp, rdi
    push 0x0       ;SS
    push rdi       ;RSP
    push 0x202     ;RFLAGS
    push 0x8       ;CS
    push rsi       ;RIP
    push 0x1       ;rax
    push 0x2       ;rbx
    push 0x3       ;rcx
    push r8        ;rdx
    push rdi       ;rbp
    push rdx       ;rdi
    push rcx       ;rsi
    push 0x0       ;r8
    push 0x9       ;r9
    push 0xA       ;r10
    push 0xb       ;r11
    push 0xc       ;r12
    push 0xd       ;r13
    push 0xe       ;r14
    push 0xf       ;r15

    mov rsp, rbp
    pop rbp
    ret

;_changeProcess:
;	mov rsp, rdi
;	popState;
;	iretq



    
