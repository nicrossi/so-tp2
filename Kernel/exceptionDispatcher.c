// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <stdint.h>

#include <lib.h>
#include <screen.h>

#define REGISTER_NUM 15

typedef void (*exception_ptr)(uint64_t *instructionPointer, uint64_t *stackPointer);

static void zero_division(uint64_t *instructionPointer, uint64_t *stackPointer);
static void invalid_opcode(uint64_t *instructionPointer, uint64_t *stackPointer);
static void write_registers(uint64_t *instructionPointer, uint64_t *stackPointer);

exception_ptr exceptions[] = {zero_division, 0, 0, 0, 0, 0, invalid_opcode};

void exceptionDispatcher(int exceptionNum, uint64_t *instructionPointer, uint64_t *stackPointer)
{
	exceptions[exceptionNum](instructionPointer, stackPointer);
}

static void zero_division(uint64_t *instructionPointer, uint64_t *stackPointer)
{
	put_string("ERROR: DIVISION BY ZERO\n", C_RED);
	write_registers(instructionPointer, stackPointer);
}

static void invalid_opcode(uint64_t *instructionPointer, uint64_t *stackPointer)
{
	put_string("ERROR: INVALID OPCODE\n", C_RED);
	write_registers(instructionPointer, stackPointer);
}

static void write_registers(uint64_t *instructionPointer, uint64_t *stackPointer)
{
	put_string("RIP: ", C_RED);
	put_int((uint64_t)instructionPointer, 16, C_RED);
	put_string("\n", C_RED);
	char *registers[] = {"R15", "R14", "R13", "R12", "R11", "R10", "R9", "R8", "RSI", "RDI", "RBP", "RDX", "RCX", "RBX", "RAX"};
	int i;
	for (i = 0; i < REGISTER_NUM; i++)
	{
		put_string(registers[i], C_RED);
		put_string(": ", C_RED);
		put_int(*(stackPointer + i), 16, C_RED);
		put_string("\n", C_RED);
	}
}
