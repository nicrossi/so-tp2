// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#ifdef MM_FREE_LIST

#include "heap_interface.h"

#include <screen.h>
static uint8_t *heap = (uint8_t *)HEAP_START_ADRESS;

typedef struct BLOCK_LINK
{
    struct BLOCK_LINK *next; /*<< The next free block in the list. */
    size_t blockSize;        /*<< The size of the free block. */
} BlockLink_t;

static const size_t heapStructSize = (sizeof(BlockLink_t) + ((size_t)(portBYTE_ALIGNMENT - 1))) & ~((size_t)portBYTE_ALIGNMENT_MASK);
#define heapMINIMUM_BLOCK_SIZE ((size_t)(heapStructSize * 2))
/* Assumes 8bit bytes! */
#define heapBITS_PER_BYTE ((size_t)8)

/* Create a couple of list links to mark the start and end of the list. */
static BlockLink_t freeListStart, *freeListEnd = NULL;

/* Keeps track of the number of free bytes remaining, but says nothing about
 * fragmentation. */
static size_t freeBytesRemaining = 0U;
static size_t numberOfSuccessfulAllocations = 0;
static size_t numberOfSuccessfulFrees = 0;
static size_t blockAllocatedBit = 0;

static void insertBlockIntoFreeList(BlockLink_t *blockToInsert)
{
    BlockLink_t *pxIterator;
    uint8_t *puc;

    /* Iterate through the list until a block is found that has a higher address
     * than the block being inserted. */
    for (pxIterator = &freeListStart; pxIterator->next < blockToInsert; pxIterator = pxIterator->next)
    {
        /* Nothing to do here, just iterate to the right position. */
    }

    /* Do the block being inserted, and the block it is being inserted after
     * make a contiguous block of memory? */
    puc = (uint8_t *)pxIterator;

    if ((puc + pxIterator->blockSize) == (uint8_t *)blockToInsert)
    {
        pxIterator->blockSize += blockToInsert->blockSize;
        blockToInsert = pxIterator;
    }
    else
    {
        //mtCOVERAGE_TEST_MARKER();
    }

    /* Do the block being inserted, and the block it is being inserted before
     * make a contiguous block of memory? */
    puc = (uint8_t *)blockToInsert;

    if ((puc + blockToInsert->blockSize) == (uint8_t *)pxIterator->next)
    {
        if (pxIterator->next != freeListEnd)
        {
            /* Form one big block from the two blocks. */
            blockToInsert->blockSize += pxIterator->next->blockSize;
            blockToInsert->next = pxIterator->next->next;
        }
        else
        {
            blockToInsert->next = freeListEnd;
        }
    }
    else
    {
        blockToInsert->next = pxIterator->next;
    }

    /* If the block being inserted plugged a gab, so was merged with the block
     * before and the block after, then it's pxNextFreeBlock pointer will have
     * already been set, and should not be set here as that would make it point
     * to itself. */
    if (pxIterator != blockToInsert)
    {
        pxIterator->next = blockToInsert;
    }
    else
    {
        //mtCOVERAGE_TEST_MARKER();
    }
}

static void heapInit()
{
    BlockLink_t *firstFreeBlock;
    uint8_t *pucAlignedHeap;
    size_t uxAddress;
    size_t xTotalHeapSize = TOTAL_HEAP_SIZE;
    /* Ensure the heap starts on a correctly aligned boundary. */
    uxAddress = (size_t)heap;

    if ((uxAddress & portBYTE_ALIGNMENT_MASK) != 0)
    {
        uxAddress += (portBYTE_ALIGNMENT - 1);
        uxAddress &= ~((size_t)portBYTE_ALIGNMENT_MASK);
        xTotalHeapSize -= uxAddress - (size_t)heap;
    }

    pucAlignedHeap = (uint8_t *)uxAddress;

    /* xStart is used to hold a pointer to the first item in the list of free
     * blocks.  The void cast is used to prevent compiler warnings. */
    freeListStart.next = (void *)pucAlignedHeap;
    freeListStart.blockSize = (size_t)0;

    /* pxEnd is used to mark the end of the list of free blocks and is inserted
     * at the end of the heap space. */
    uxAddress = ((size_t)pucAlignedHeap) + xTotalHeapSize;
    uxAddress -= heapStructSize;
    uxAddress &= ~((size_t)portBYTE_ALIGNMENT_MASK);
    freeListEnd = (void *)uxAddress;
    freeListEnd->blockSize = 0;
    freeListEnd->next = NULL;

    /* To start with there is a single free block that is sized to take up the
     * entire heap space, minus the space taken by pxEnd. */
    firstFreeBlock = (void *)pucAlignedHeap;
    firstFreeBlock->blockSize = uxAddress - (size_t)firstFreeBlock;
    firstFreeBlock->next = freeListEnd;

    /* Only one block exists - and it covers the entire usable heap space. */
    //xMinimumEverFreeBytesRemaining = pxFirstFreeBlock->blockSize;
    freeBytesRemaining = firstFreeBlock->blockSize;

    /* Work out the position of the top bit in a size_t variable. */
    blockAllocatedBit = ((size_t)1) << ((sizeof(size_t) * heapBITS_PER_BYTE) - 1);
}

void *malloc(size_t wantedSize)
{
    BlockLink_t *block;

    void *heapReturn = NULL;

    /* If this is the first call to malloc then the heap will require
         * initialisation to setup the list of free blocks. */
    if (freeListEnd == NULL)
    {
        heapInit();
    }

    if ((wantedSize & blockAllocatedBit) == 0)
    {
        if (wantedSize > 0)
        {
            wantedSize += heapStructSize;

            /* Ensure that blocks are always aligned to the required number of bytes. */
            if ((wantedSize & portBYTE_ALIGNMENT_MASK) != 0x00)
            {
                /* Byte alignment required. */
                wantedSize += (portBYTE_ALIGNMENT - (wantedSize & portBYTE_ALIGNMENT_MASK));
            }
        }

        /* Check the requested block size is not so large that the top bit is
         * set.  The top bit of the block size member of the BlockLink_t structure
         * is used to determine who owns the block - the application or the
         * kernel, so it must be free. */
        if ((wantedSize > 0) && (wantedSize <= freeBytesRemaining))
        {
            /* Blocks are stored in byte order - traverse the list from the start
             * (smallest) block until one of adequate size is found. */
            BlockLink_t *previousBlock = &freeListStart;
            block = freeListStart.next;

            while ((block->blockSize < wantedSize) && (block->next != NULL))
            {
                previousBlock = block;
                block = block->next;
            }

            /* If we found the end marker then a block of adequate size was not found. */
            if (block != freeListEnd)
            {
                /* Return the memory space - jumping over the BlockLink_t structure
                         * at its start. */
                heapReturn = (void *)(((uint8_t *)previousBlock->next) + heapStructSize);

                /* This block is being returned for use so must be taken out of the
                         * list of free blocks. */
                previousBlock->next = block->next;

                /* If the block is larger than required it can be split into two. */
                if ((block->blockSize - wantedSize) > heapMINIMUM_BLOCK_SIZE)
                {
                    /* This block is to be split into two.  Create a new block
                             * following the number of bytes requested. The void cast is
                             * used to prevent byte alignment warnings from the compiler. */
                    BlockLink_t *newBlock = (void *)(((uint8_t *)block) + wantedSize);

                    /* Calculate the sizes of two blocks split from the single
                             * block. */
                    newBlock->blockSize = block->blockSize - wantedSize;
                    block->blockSize = wantedSize;

                    /* Insert the new block into the list of free blocks. */
                    insertBlockIntoFreeList(newBlock);
                }

                freeBytesRemaining -= block->blockSize;
                /* The block is being returned - it is allocated and owned
                             * by the application and has no "next" block. */
                block->blockSize |= blockAllocatedBit;
                block->next = NULL;
                numberOfSuccessfulAllocations++;
            }
        }
    }
    /* The wanted size is increased so it can contain a BlockLink_t
                 * structure in addition to the requested amount of bytes. */

    return heapReturn;
}

void free(void *heapPointer)
{
    uint8_t *hp = (uint8_t *)heapPointer;

    if (heapPointer != NULL)
    {
        /* The memory being freed will have an BlockLink_t structure immediately
         * before it. */
        hp -= heapStructSize;

        /* This unexpected casting is to keep some compilers from issuing
         * byte alignment warnings. */
        BlockLink_t *link = (void *)hp;

        if ((link->blockSize & blockAllocatedBit) != 0)
        {
            if (link->next == NULL)
            {
                /* The block is being returned to the heap - it is no longer
                 * allocated. */
                link->blockSize &= ~blockAllocatedBit;
            }
        }
        /* Add this block to the list of free blocks. */
        freeBytesRemaining += link->blockSize;
        insertBlockIntoFreeList(((BlockLink_t *)link));
        numberOfSuccessfulFrees++;
    }
}
/*-----------------------------------------------------------*/

void getHeapStats(HeapStats_t *stats)
{
    BlockLink_t *block;
    size_t xBlocks = 0, xMaxSize = 0, xMinSize = TOTAL_HEAP_SIZE; /* portMAX_DELAY used as a portable way of getting the maximum value. */

    // vTaskSuspendAll();
    {
        block = freeListStart.next;

        /* pxBlock will be NULL if the heap has not been initialised.  The heap
         * is initialised automatically when the first allocation is made. */
        if (block != NULL)
        {
            do
            {
                /* Increment the number of blocks and record the largest block seen
                 * so far. */
                xBlocks++;

                if (block->blockSize > xMaxSize)
                {
                    xMaxSize = block->blockSize;
                }

                if (block->blockSize < xMinSize)
                {
                    xMinSize = block->blockSize;
                }

                /* Move to the next block in the chain until the last block is
                 * reached. */
                block = block->next;
            } while (block != freeListEnd);
        }
    }
    //(void)xTaskResumeAll();

    stats->sizeOfLargestFreeBlockInBytes = xMaxSize;
    stats->sizeOfSmallestFreeBlockInBytes = xMinSize;
    stats->numberOfFreeBlocks = xBlocks;

    // taskENTER_CRITICAL();
    {
        stats->totalHeapSizeInBytes = TOTAL_HEAP_SIZE;
        size_t occupiedHeapSpaceInBytes = TOTAL_HEAP_SIZE - freeBytesRemaining;
        stats->occupiedHeapSpaceInBytes = occupiedHeapSpaceInBytes;
        stats->availableHeapSpaceInBytes = freeBytesRemaining;
        stats->numberOfSuccessfulAllocations = numberOfSuccessfulAllocations;
        stats->numberOfSuccessfulFrees = numberOfSuccessfulFrees;
    }
    //   taskEXIT_CRITICAL();
}

#endif