#ifndef BEEP_H
#define BEEP_H

//activate sound
void _beep();

//deactivate sound
void _unbeep();

#endif