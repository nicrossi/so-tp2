#ifndef HEAP_INTERFACE_H_
#define HEAP_INTERFACE_H_

#include <stdint.h>
#include <stddef.h>
#define TOTAL_HEAP_SIZE (1024 * 1024 * 64) // 64MB
#define HEAP_START_ADRESS 0x700000
#define portBYTE_ALIGNMENT 8
#define portBYTE_ALIGNMENT_MASK 7
typedef struct HeapStats
{
    size_t totalHeapSizeInBytes;           /*The total heap size*/
    size_t occupiedHeapSpaceInBytes;       /* The total heap size currently occupied*/
    size_t availableHeapSpaceInBytes;      /* The total heap size currently available - this is the sum of all the free blocks, not the largest block that can be allocated. */
    size_t sizeOfLargestFreeBlockInBytes;  /* The maximum size, in bytes, of all the free blocks within the heap at the time vPortGetHeapStats() is called. */
    size_t sizeOfSmallestFreeBlockInBytes; /* The minimum size, in bytes, of all the free blocks within the heap at the time vPortGetHeapStats() is called. */
    size_t numberOfFreeBlocks;             /* The number of free memory blocks within the heap at the time vPortGetHeapStats() is called. */
    size_t numberOfSuccessfulAllocations;  /* The number of calls to pvPortMalloc() that have returned a valid memory block. */
    size_t numberOfSuccessfulFrees;        /* The number of calls to vPortFree() that has successfully freed a block of memory. */
} HeapStats_t;

void getHeapStats(HeapStats_t *stats);
void *malloc(size_t wantedSize);
void free(void *heapPointer);

#endif
