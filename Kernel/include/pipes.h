#ifndef _PIPES_H
#define _PIPES_H
#include <stddef.h>
#include <sys/types.h>
#include <semaphore.h>
#define PIPE_MAX_SIZE 256

// typedef struct Procces_Node_t
// {
//     pid_t pid;
//     struct Process_Node_t *next;
// } Process_Node_t;

#define STD_IN 0
#define STD_OUT 0
typedef struct Pipe_t
{
    uint64_t id;

    char buff[PIPE_MAX_SIZE];
    uint64_t nread, nwrite;
    Semaphore_t *read_sem;
    Semaphore_t *write_sem;
    struct Process_Node_t *process_list;
} Pipe_t;

uint64_t openPipe();
uint64_t closePipe(uint64_t id);
void openPipeForProcess(uint64_t id, pid_t pid);
void closePipeForProcess(int64_t id, pid_t pid);
void printPipes();
Pipe_t *getPipe(uint64_t id);
uint64_t readPipe(char *addr, uint64_t id);
uint64_t writePipe(char *addr, uint64_t id);
uint64_t readStd(char *addr);
uint64_t writeStd(char *addr);
#endif