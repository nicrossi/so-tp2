#ifndef SCHEDULER_H
#define SCHEDULER_H
#include <sys/types.h>
#include <heap_interface.h>

#define MAX_PRIORITY_EXCLUSIVE 14
#define MAX_PROCESS_NAME 48
#define BG 1
#define FG 0

enum ProcessState
{
    READY,
    BLOCKED,
    KILLED,
    INIT
};

typedef struct ProcessControlBlock
{
    char name[MAX_PROCESS_NAME];
    uint64_t priority;
    enum ProcessState state;
    int type; // 0 = foreground, 1 = background
    pid_t pid;
    pid_t ppid;
    void *stack_start;
    void *rsp;
    uint64_t std_output;
    uint64_t std_input;
    char blocked_by_sem;
} ProcessControlBlock_t;

typedef struct ProcessQueueNode
{
    ProcessControlBlock_t pcb;
    uint64_t quantums_left;
    struct ProcessQueueNode *prev;
    struct ProcessQueueNode *next;
} ProcessQueueNode_t;

//-------------Funciones de inicializacion de cosas------------------------------                                       //
pid_t createProcess(char *name, int argc, char *argv[], void *entry_point, enum ProcessState state, uint64_t *parameters);
void initScheduler();
//-------------Funciones de acciones que puede realizar el scheduler-------------

void *schedule(void *old_rsp);
void freeKilledQueue();

//--------------Funciones para control de los procesos---------------------------

void printPCB(ProcessControlBlock_t pcb);
void ps();

int killProcessByPID(pid_t pid);
int changeState(uint64_t pid, enum ProcessState state);
int changePriority(pid_t pid, uint64_t priority);
pid_t getPID();

int unlockFromSem(uint64_t pid);
int lockToSem(uint64_t pid);
ProcessQueueNode_t *getProcessByPid();
void yieldProcess();
#endif
