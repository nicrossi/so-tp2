
#ifndef SCREEN_H
#define SCREEN_H
#include <stdint.h>
#include <video_driver.h>
#include <pipes.h>
extern const Color C_BLACK;
extern const Color C_RED;
extern const Color C_GREEN;
extern const Color C_BLUE;
extern const Color C_WHITE;
//writes a char of the given color with a starting position of x,y.
void write_char(char c, uint64_t x, uint64_t y, Color color);

void put_string(char *s, Color color);
void put_dec(int value, Color color);
void put_int(int value, int base, Color color);
void write_char_on_screen(char c, Color color);
int get_screen_width();
int get_screen_height();
void clear_screen();
#endif