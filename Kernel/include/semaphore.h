#ifndef _SEMAPHORE_H
#define _SEMAPHORE_H

#include <stdint.h>
#include <stddef.h>

#include <scheduler.h>

#define SEMAPHORE_NAME 256

enum semState
{
    OPEN,
    CLOSE,
    WAIT,
    POST,
    PRINTALL
};

typedef struct Process_Node_t
{
    uint64_t pid;
    uint64_t is_blocked;
    struct Process_Node_t *next;
} Process_Node_t;

typedef struct Semaphore_t
{
    char *name;
    uint64_t id;
    uint64_t count;
    uint64_t lock;
    Process_Node_t *processes;
} Semaphore_t;

typedef struct Sem_Node_t
{
    Semaphore_t *semaphore;
    struct Sem_Node_t *next;
} Sem_Node_t;

int sys_semaphore(void *option, void *arg1, void *arg2, void *arg3, void *arg4);

void semOpen(char *name, uint64_t pid, uint64_t start_cont, uint64_t *resp_id);
Semaphore_t *semOpenEmpty(uint64_t start_cont);
void addProcessToSem(struct Semaphore_t *sem, uint64_t pid);
struct Sem_Node_t *semInit(char *name, uint64_t pid, uint64_t value);

void semClose(uint64_t id, uint64_t pid, uint64_t *resp);
void removeProcessFromSem(struct Semaphore_t *sem, uint64_t pid);

void semWait(uint64_t id, uint64_t pid, uint64_t *resp);
struct Semaphore_t *getSem(uint64_t id);
struct Process_Node_t *getProcess(struct Semaphore_t *sem, uint64_t pid);

void semPost(uint64_t id, uint64_t *resp);
void unlockFirstBlockedProcess(Semaphore_t *sem);

void semPrintAll();
void printSem(struct Semaphore_t *sem);
void printSemProcesses(struct Process_Node_t *process);

void spinUnlock(uint64_t *lock);
void spinLock(uint64_t *lock);

#endif