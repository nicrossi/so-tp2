// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <time.h>

#include <keyboard_driver.h>
#include <scheduler.h>
#include <interrupts.h>

static void int_21();

void (*irq_handler[])() = {int_21};

void irqDispatcher(uint64_t irq)
{
	switch (irq)
	{
	case 1:
		int_21();
		break;

	default:
		break;
	}
}

void *int_20(void *old_rsp)
{

	void *new_rsp = schedule(old_rsp);
	freeKilledQueue();
	timer_handler();
	return new_rsp;
}

void int_21()
{

	keyboard_handler();
}
