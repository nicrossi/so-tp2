// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <pipes.h>
#include <heap_interface.h>

#include <scheduler.h>
#include <screen.h>

#define MAX_PIPES 128
#define STD_OUT 0
#define STD_ERROR 1
static struct Pipe_t *createPipe();
static void printPipe(struct Pipe_t *pipe);
static uint64_t getFirstAvailableId();

// static struct Pipe_node_t *pipe_list_first;
static struct Pipe_t *pipe_table[MAX_PIPES];

Pipe_t *getPipe(uint64_t id)
{
    if (id < MAX_PIPES)
    {
        return pipe_table[id];
    }
    return NULL;
}

uint64_t readStd(char *addr)
{
    uint64_t pid = getPID();
    uint64_t stdin = (getProcessByPid(pid))->pcb.std_input;

    return readPipe(addr, stdin);
}
uint64_t writeStd(char *addr)
{
    uint64_t pid = getPID();
    uint64_t stdout = (getProcessByPid(pid))->pcb.std_output;

    return writePipe(addr, stdout);
}

uint64_t readPipe(char *addr, uint64_t id)
{
    if (id < MAX_PIPES)
    {
        Pipe_t *pipe = pipe_table[id];
        if (pipe != NULL)
        {

            pid_t pid = getPID();
            addProcessToSem(pipe->read_sem, pid);
            uint64_t sem_resp;

            semWait(pipe->read_sem->id, pid, &sem_resp);

            *addr = pipe->buff[pipe->nread++ % PIPE_MAX_SIZE];
            semPost(pipe->write_sem->id, &sem_resp);

            removeProcessFromSem(pipe->read_sem, pid);
            return 1;
        }
    }
    return -1;
}
uint64_t writePipe(char *addr, uint64_t id)
{
    if (id == STD_OUT)
    {

        write_char_on_screen(*addr, C_WHITE);
        return 1;
    }
    else if (id == STD_ERROR)
    {
        write_char_on_screen(*addr, C_RED);
        return 1;
    }
    if (id < MAX_PIPES)
    {
        Pipe_t *pipe = pipe_table[id];
        if (pipe != NULL)
        {

            pid_t pid = getPID();
            addProcessToSem(pipe->write_sem, pid);
            uint64_t sem_resp;
            semWait(pipe->write_sem->id, pid, &sem_resp);
            pipe->buff[pipe->nwrite++ % PIPE_MAX_SIZE] = *addr;
            semPost(pipe->read_sem->id, &sem_resp);
            removeProcessFromSem(pipe->write_sem, pid);
            return 1;
        }
        return -1;
    }
    return -1;
}

uint64_t openPipe()
{
    Pipe_t *pipe = createPipe();
    if (pipe == NULL)
    {
        return -1;
    }
    return pipe->id;
}

uint64_t closePipe(uint64_t id)
{
    if (id >= 0 && id < MAX_PIPES)
    {
        Pipe_t *pipe = pipe_table[id];
        if (pipe != NULL)
        {
            pid_t pid = getPID();
            if (pid != 0)
            {
                closePipeForProcess(id, pid);
                if (pipe->process_list == NULL)
                {
                    free((void *)pipe);
                    pipe_table[id] = NULL;
                }
            }

            return 1;
        }
    }

    return -1;
}

static Pipe_t *createPipe()
{

    Pipe_t *pipe = (Pipe_t *)malloc(sizeof(Pipe_t));
    if (pipe == NULL)
    {
        return NULL;
    }
    int64_t id = getFirstAvailableId();
    if (id == -1)
    {
        free((void *)pipe);
        return NULL;
    }
    pipe->id = id;
    pipe->read_sem = semOpenEmpty(0);
    pipe->write_sem = semOpenEmpty(PIPE_MAX_SIZE);
    pipe_table[id] = pipe;

    return pipe;
}

void openPipeForProcess(uint64_t id, pid_t pid)
{
    if (id < MAX_PIPES)
    {
        Pipe_t *pipe = pipe_table[id];
        if (pipe != NULL)
        {
            if (pid != 0)
            {
                Process_Node_t *process = (Process_Node_t *)malloc(sizeof(Process_Node_t));
                if (process != NULL)
                {
                    Process_Node_t *first = pipe->process_list;
                    if (first == NULL)
                    {
                        pipe->process_list = process;
                        process->next = NULL;
                    }
                    else
                    {
                        pipe->process_list = process;
                        process->next = first;
                    }
                }
            }
        }
    }
}

void closePipeForProcess(int64_t id, pid_t pid)
{
    if (id < MAX_PIPES)
    {
        Pipe_t *pipe = pipe_table[id];
        if (pid != 0)
        {
            struct Process_Node_t *iterator = pipe->process_list;
            struct Process_Node_t *prev = NULL;
            while (iterator != NULL)
            {
                if (iterator->pid == pid)
                {
                    if (prev == NULL)
                    {
                        if (iterator->next != NULL)
                        {
                            pipe->process_list = iterator->next;
                        }
                        else
                        {
                            pipe->process_list = NULL;
                        }
                    }
                    else
                    {
                        prev->next = iterator->next;
                    }
                    free((void *)iterator);
                    return;
                }
                prev = iterator;
                iterator = iterator->next;
            }
        }
    }
}

static uint64_t getFirstAvailableId()
{
    for (int i = 0; i < MAX_PIPES; i++)
    {
        if (pipe_table[i] == NULL)
        {
            return i;
        }
    }
    return -1;
}

void printPipes()
{
    put_string("-------------------------PIPES IFORMATION-----------------------", C_WHITE);
    put_string("\n", C_WHITE);
    for (int i = 0; i < MAX_PIPES; i++)
    {

        Pipe_t *pipe = pipe_table[i];
        if (pipe != NULL)
        {
            printPipe(pipe);
        }
    }

    put_string("----------------------------------------------------------------", C_WHITE);
    put_string("\n", C_WHITE);
}
static void printPipe(Pipe_t *pipe)
{

    put_string("| id: ", C_WHITE);
    put_dec(pipe->id, C_WHITE);
    put_string(" | ", C_WHITE);
    put_string("\n", C_WHITE);
}