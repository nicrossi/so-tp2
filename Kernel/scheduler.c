// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "scheduler.h"

#include "pipes.h"
#include <screen.h>

#define PROCESS_STACK_TOTAL_SIZE 1024 // 10KB
#define DEFAULT_PRIORITY 12
#define SUCCESS 1
#define NOT_FOUND_ERROR 2
#define NO_SPACE_ERROR 3
#define DEFAULT_ERROR 4
#define BG 1
#define FG 0
typedef int (*EntryPoint)(int, char **);

typedef void (*wrapperFunction)(int argc, char *argv[], EntryPoint entry_point);

static ProcessQueueNode_t *current_process = NULL;

static ProcessQueueNode_t *init_process = NULL;

static uint64_t proccess_count = 0;
static uint64_t started = 0;

void addProcessToActiveQueue(ProcessQueueNode_t *node);
void _configNewStack(void *bp, void *funcWrapper, int argc, char **argv, void *entry_point);
void runEntryPoint(int argc, char **argv, EntryPoint entry_point);
void _sti_and_hlt();
void killProcess(ProcessQueueNode_t *to_kill);
void blockCurrent();
void initProcess();
void yield();
ProcessQueueNode_t *getNextProcess();
char *getStateText(int state);

pid_t getPID()
{
    if (current_process == NULL)
    {
        return 0;
    }
    return current_process->pcb.pid;
}
void initScheduler()
{
    char *argv[1];
    argv[0] = "init";
    uint64_t std_pipe = openPipe();
    //Create std error
    openPipe();
    //
    void (*init)(void);
    init = &initProcess;
    uint64_t parameters[] = {0, BG, std_pipe, std_pipe};

    createProcess("init", 1, argv, init, INIT, parameters);
    _sti_and_hlt();
}

void initProcess(int argc, char *argv[])
{

    char *argv_shell[1];
    argv_shell[0] = "shell";
    uint64_t parameters[] = {0, FG, 0, 0};

    createProcess("shell", 1, argv_shell, (void *)0x400000, READY, parameters);

    _sti_and_hlt();
    while (1)
    {

        _sti_and_hlt();
    };
}

void yieldProcess()
{
    current_process->quantums_left = 0;
    yield();
}

void blockCurrent()
{
    changeState(current_process->pcb.pid, BLOCKED);
}

void *schedule(void *old_rsp)
{
    if (started == 0)
    {
        if (current_process == NULL)
        {
            return old_rsp;
        }

        started = 1;
        return current_process->pcb.rsp;
    }
    else
    {
        current_process->quantums_left--;

        if ((uint64_t)old_rsp >= HEAP_START_ADRESS)
        {
            current_process->pcb.rsp = old_rsp;
        }

        if (current_process->pcb.state != READY || current_process->quantums_left == 0 || current_process->pcb.blocked_by_sem == 1)
        {

            current_process->quantums_left = (MAX_PRIORITY_EXCLUSIVE - current_process->pcb.priority);
            current_process = getNextProcess();
        }

        return current_process->pcb.rsp;
    }
}

ProcessQueueNode_t *getProcessByPid(pid_t pid)
{

    ProcessQueueNode_t *aux = current_process;

    if (aux != NULL)
    {
        int counter = 0;
        while (counter < proccess_count)
        {

            if (aux->pcb.pid == pid)
            {
                return aux;
            }

            aux = aux->next;
            counter++;
        }

        return NULL;
    }

    return NULL;
}

ProcessQueueNode_t *getNextProcess()
{

    ProcessQueueNode_t *aux = current_process->next;

    if (aux != NULL)
    {
        int counter = 0;
        while (counter < proccess_count)
        {
            if (aux->pcb.state == READY && aux->pcb.blocked_by_sem == 0)
            {

                return aux;
            }
            aux = aux->next;
            counter++;
        }

        return init_process;
    }

    return NULL;
}

pid_t createProcess(char *name, int argc, char *argv[], void *entry_point, enum ProcessState state, uint64_t *parameters)
{
    ProcessQueueNode_t *new_process = (ProcessQueueNode_t *)malloc(sizeof(ProcessQueueNode_t));
    if (new_process == NULL)
    {

        put_string("No space in stack\n", C_RED);
        return -1;
    }

    void *stack_start = (void *)malloc(PROCESS_STACK_TOTAL_SIZE * 8);
    if (stack_start == NULL)
    {
        free((void *)new_process);
        put_string("No space in stack\n", C_RED);
        return -1;
    }

    proccess_count++;

    int i = 0;
    while (name[i] != '\0')
    {
        new_process->pcb.name[i] = name[i];
        if (i == MAX_PROCESS_NAME - 1)
        {
            new_process->pcb.name[i] = '\0';
            break;
        }
        i++;
    }
    new_process->pcb.name[i] = '\0';

    new_process->pcb.pid = (pid_t)proccess_count;
    new_process->pcb.ppid = (pid_t)current_process->pcb.pid;
    new_process->pcb.stack_start = stack_start;
    void *rbp = (void *)((uint64_t *)stack_start + PROCESS_STACK_TOTAL_SIZE);

    new_process->pcb.rsp = (void *)((uint64_t *)rbp - 20);
    new_process->pcb.state = state;

    uint64_t priority = parameters[0];
    uint64_t type = parameters[1];

    uint64_t std_input = parameters[2];
    uint64_t std_output = parameters[3];
    new_process->pcb.priority = priority;
    new_process->pcb.type = type;
    new_process->pcb.std_input = std_input;
    new_process->pcb.std_output = std_output;
    new_process->pcb.blocked_by_sem = 0;
    openPipeForProcess(std_input, proccess_count);
    openPipeForProcess(std_output, proccess_count);
    new_process->quantums_left = (MAX_PRIORITY_EXCLUSIVE - priority);

    if (type == FG && current_process->pcb.type == FG)
    {

        blockCurrent();
    }

    wrapperFunction funcWrapper;
    funcWrapper = &runEntryPoint;
    _configNewStack(rbp, funcWrapper, argc, argv, entry_point);
    addProcessToActiveQueue(new_process);

    return new_process->pcb.pid;
}

void runEntryPoint(int argc, char *argv[], EntryPoint entry_point)
{
    entry_point(argc, argv);
    killProcess(current_process);
    // put_string("TERMINO PROCESO PID: ", C_WHITE);
    // put_dec(current_process->pcb.pid, C_WHITE);
    // put_string("\n", C_WHITE);
    _sti_and_hlt();
    // /* DEBUG */ put_string("KILLED:THIS SHOULD NEVER PRINT\n", C_WHITE);
    while (1)
    {
    };
}

void addProcessToActiveQueue(ProcessQueueNode_t *node)
{
    if (current_process == NULL)
    {
        current_process = node;
        current_process->next = current_process;
        current_process->prev = current_process;

        if (current_process->pcb.state == INIT)
        {
            init_process = current_process;
        }
    }
    else
    {
        node->next = current_process;
        node->prev = current_process->prev;
        current_process->prev->next = node;
        current_process->prev = node;
    }
}

void freeKilledQueue()
{
    int counter = 0;
    int killed = 0;
    ProcessQueueNode_t *aux = current_process;

    while (counter < proccess_count)
    {
        if (aux->pcb.state == KILLED)
        {
            ProcessQueueNode_t *parent = getProcessByPid(aux->pcb.ppid);

            if (parent != NULL && parent->pcb.type == FG && parent->pcb.state == BLOCKED)
            {
                parent->pcb.state = READY;
            }
            aux->prev->next = aux->next;
            aux->next->prev = aux->prev;
            killed++;
            closePipe(aux->pcb.std_output);
            closePipe(aux->pcb.std_input);
            free((void *)aux->pcb.stack_start);
            free((void *)aux);
        }

        aux = aux->next;

        counter++;
    }
    proccess_count -= killed;
}

int killProcessByPID(pid_t pid)
{
    ProcessQueueNode_t *aux = getProcessByPid(pid);
    if (aux != NULL)
    {
        killProcess(aux);
        return SUCCESS;
    }

    return -1;
}

void killProcess(ProcessQueueNode_t *to_kill)
{

    to_kill->pcb.state = KILLED;

    return;
}

int changePriority(pid_t pid, uint64_t priority)
{
    if (priority < MAX_PRIORITY_EXCLUSIVE)
    {
        ProcessQueueNode_t *aux = getProcessByPid(pid);
        if (aux != NULL)
        {
            aux->pcb.priority = priority;
            return SUCCESS;
        }

        return -1;
    }

    return -1;
}

int changeState(uint64_t pid, enum ProcessState state)
{
    ProcessQueueNode_t *aux = getProcessByPid(pid);
    if (aux != NULL)
    {
        aux->pcb.state = state;
        return SUCCESS;
    }

    return -1;
}

// Print all processes
void ps()
{
    int i;

    ProcessQueueNode_t *aux = current_process;
    for (i = 0; aux != NULL && i < proccess_count; i++, aux = aux->next)
    {
        if (aux->pcb.state != KILLED)
        {
            printPCB(aux->pcb);
        }
    }
}

void printPCB(ProcessControlBlock_t pcb)
{
    put_string("NAME: ", C_WHITE);
    put_string(pcb.name, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("PID: ", C_WHITE);
    put_dec(pcb.pid, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("RBP: ", C_WHITE);
    put_dec(((uint64_t)pcb.stack_start + PROCESS_STACK_TOTAL_SIZE + 1), C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("RSP: ", C_WHITE);
    put_dec((uint64_t)pcb.rsp, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("PRIORITY: ", C_WHITE);
    put_dec(pcb.priority, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("TYPE: ", C_WHITE);
    pcb.type == FG ? (put_string("FG", C_WHITE)) : (put_string("BG", C_WHITE));
    put_string("\t|\t", C_WHITE);
    put_string("STATE: ", C_WHITE);
    put_string(getStateText(pcb.state), C_WHITE);
    put_dec(pcb.state, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("BLOCKED BY SEM: ", C_WHITE);
    put_dec(pcb.blocked_by_sem, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("STD_OUT: ", C_WHITE);
    put_dec(pcb.std_output, C_WHITE);
    put_string("\t|\t", C_WHITE);
    put_string("STD_IN: ", C_WHITE);
    put_dec(pcb.std_input, C_WHITE);
    put_string("\n", C_WHITE);
}

char *getStateText(int state)
{
    switch (state)
    {
    case READY:
        return "READY ";
    case BLOCKED:
        return "BLOCKED ";
    case KILLED:
        return "KILLED ";
    case INIT:
        return "INIT ";
    default:
        return "NOT_FOUND";
    }
}

int unlockFromSem(uint64_t pid)
{

    int counter = 0;
    ProcessQueueNode_t *aux = current_process;
    while (counter < proccess_count)
    {
        if (aux->pcb.pid == pid)
        {

            aux->pcb.blocked_by_sem = 0;

            return 0;
        }
        aux = aux->next;
        counter++;
    }
    return -1;
}

int lockToSem(uint64_t pid)
{
    int counter = 0;
    ProcessQueueNode_t *aux = current_process;

    while (counter < proccess_count)
    {
        if (aux->pcb.pid == pid)
        {

            aux->pcb.blocked_by_sem = 1;
            return 0;
        }
        aux = aux->next;
        counter++;
    }
    return -1;
}