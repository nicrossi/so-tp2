// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <font.h>
#include <stdint.h>

#include <screen.h>
#define HORIZONTAL_MARGIN 2
#define VERTICAL_MARGIN 0
#define BACKSPACE '\b'
#define SPACE ' '
#define NEW_LINE '\n'
const Color C_BLACK = {0, 0, 0};
const Color C_RED = {255, 0, 0};
const Color C_GREEN = {0, 255, 0};
const Color C_BLUE = {0, 0, 255};
const Color C_WHITE = {255, 255, 255};
static int SCREEN_WIDTH = -1;
static int SCREEN_HEIGHT = -1;

static int line_y_value = VERTICAL_MARGIN;
static int line_x_value = HORIZONTAL_MARGIN;

static int invalid_position_y();
static int invalid_position_x();

static void reset_x_position();
static void reset_y_position();
static void new_line();
static void set_cursor();
static int get_maxline_x();
static int get_maxline_y();

static int get_maxline_x()
{
    return (get_screen_width() - HORIZONTAL_MARGIN * WIDTH);
}
static int get_maxline_y()
{
    return (get_screen_height() - VERTICAL_MARGIN * HEIGHT);
}

static void new_line()
{
    if (invalid_position_y())
    {
        move_screen_up(HEIGHT, C_BLACK);
        line_y_value--;
    }
    line_y_value++;
    reset_x_position();
}

void put_dec(int value, Color color)
{
    put_int(value, 10, color);
}

void put_int(int value, int base, Color color)
{

    char number[20];
    int i = 0;
    int digits = 0;
    int negative = 0;

    if (value < 0)
    {
        value *= -1;
        negative = 1;
    }
    do
    {
        int remainder = value % base;
        number[i++] = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
        digits++;
    } while (value /= base);
    if (negative)
    {

        writeStd("-");
    }
    for (i = digits - 1; i >= 0; i--)
        writeStd(&number[i]);
}
void put_string(char *s, Color color)
{
    for (int i = 0; s[i] != '\0'; i++)
    {
        write_char_on_screen(s[i], color);
    }
}

void write_char_on_screen(char c, Color color)
{

    if (c == BACKSPACE)
    {

        if (line_x_value <= HORIZONTAL_MARGIN && line_y_value > VERTICAL_MARGIN)
        {
            line_y_value--;
            line_x_value = (get_maxline_x() / ((WIDTH + 1)));
        }
        else if (line_x_value > HORIZONTAL_MARGIN)
        {

            line_x_value--;
        }
        write_block(line_x_value * (WIDTH + 1), line_y_value * HEIGHT, WIDTH + 1, HEIGHT, C_BLACK);
    }
    else if (c == NEW_LINE)
    {
        new_line();
    }
    else
    {
        set_cursor();

        write_char(c, line_x_value * (WIDTH + 1), line_y_value * HEIGHT, color);
        line_x_value++;
    }
}

void write_char(char c, uint64_t x, uint64_t y, Color color)
{
    if (c < 32)
        return;

    char *char_pointer = getLetter(c);

    for (int j = 0; j < HEIGHT; j++)
    {
        for (int i = 0; i < WIDTH; i++)
        {
            if ((1 << i) & char_pointer[j])
                write_pixel((WIDTH - i) + x, j + y, color);
        }
    }
}



static void set_cursor()
{
    if (invalid_position_x())
        new_line();
}

int get_screen_width()
{
    if (SCREEN_WIDTH == -1)
    {
        return SCREEN_WIDTH = get_width();
    }
    return SCREEN_WIDTH;
}

int get_screen_height()
{

    if (SCREEN_HEIGHT == -1)
    {
        return SCREEN_HEIGHT = get_height();
    }
    return SCREEN_HEIGHT;
}

static int invalid_position_x()
{
    return (line_x_value) * (WIDTH + 1) > get_maxline_x();
}

static int invalid_position_y()
{
    return (line_y_value) * (HEIGHT + 1) > get_maxline_y();
}

static void reset_x_position()
{
    line_x_value = HORIZONTAL_MARGIN;
}

static void reset_y_position()
{
    line_y_value = VERTICAL_MARGIN;
}
void clear_screen()
{
    fill_screen(C_BLACK);
    reset_x_position();
    reset_y_position();
}