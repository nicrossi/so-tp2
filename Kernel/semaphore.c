// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <semaphore.h>
#include <heap_interface.h>
#include <screen.h>
#include <scheduler.h>
static int strcmp(char *s1, char *s2);

static void sleep(Process_Node_t *proc);

Sem_Node_t *first = NULL;
int next_id = 1;

int sys_semaphore(void *option, void *arg1, void *arg2, void *arg3, void *arg4)
{
    switch ((uint64_t)option)
    {
    case OPEN:
        semOpen((char *)arg1, (uint64_t)arg2, (uint64_t)arg3, (uint64_t *)arg4);
        break;
    case CLOSE:
        semClose((uint64_t)arg1, (uint64_t)arg2, (uint64_t *)arg3);
        break;

    case WAIT:
        semWait((uint64_t)arg1, (uint64_t)arg2, (uint64_t *)arg3);
        break;
    case POST:
        semPost((uint64_t)arg1, (uint64_t *)arg2);
        break;
    case PRINTALL:
        semPrintAll();
        break;
    }
    return 0;
}

void semOpen(char *name, uint64_t pid, uint64_t start_cont, uint64_t *resp_id)
{
    if (first == NULL)
    {

        first = semInit(name, pid, start_cont);

        *resp_id = first->semaphore->id;
    }
    else
    {
        Sem_Node_t *iterator = first;
        Sem_Node_t *prev = NULL;

        if (name != NULL)
        {
            while (iterator != NULL)
            {
                if (iterator->semaphore->name != NULL && strcmp(iterator->semaphore->name, name) == 0)
                {

                    *resp_id = iterator->semaphore->id;
                    addProcessToSem(iterator->semaphore, pid);
                    return;
                }
                prev = iterator;
                iterator = iterator->next;
            }
            prev->next = semInit(name, pid, start_cont);
            *resp_id = prev->next->semaphore->id;
            return;
        }
        else
        {
            first = semInit(name, pid, start_cont);
            first->next = iterator;
            *resp_id = first->semaphore->id;
        }
    }
    return;
}

void addProcessToSem(Semaphore_t *sem, uint64_t pid)
{

    Process_Node_t *new;

    if (sem->processes == NULL)
    {
        new = (Process_Node_t *)malloc(sizeof(Process_Node_t));
        if (new == NULL)
        {
            return;
        }
        new->pid = pid;
        new->is_blocked = 0;
        new->next = NULL;
        sem->processes = new;
        return;
    }
    else
    {
        Process_Node_t *prev = NULL;
        Process_Node_t *iterator = sem->processes;
        while (iterator != NULL)
        {
            if (iterator->pid == pid)
            {
                return;
            }
            prev = iterator;
            iterator = iterator->next;
        }

        new = (Process_Node_t *)malloc(sizeof(Process_Node_t));
        if (new == NULL)
        {
            return;
        }
        new->pid = pid;
        new->is_blocked = 0;
        new->next = NULL;
        prev->next = new;
    }
}
Semaphore_t *semOpenEmpty(uint64_t start_cont)
{
    uint64_t resp;
    semOpen(NULL, -1, start_cont, &resp);

    return getSem(resp);
}
Sem_Node_t *semInit(char *name, uint64_t pid, uint64_t value)
{
    Semaphore_t *new_sem = (Semaphore_t *)malloc(sizeof(Semaphore_t));
    if (new_sem == NULL)
    {
        return NULL;
    }
    new_sem->name = name;
    new_sem->id = next_id++;
    new_sem->count = value;
    new_sem->lock = 0;
    new_sem->processes = NULL;

    if (pid > 0)
    {
        addProcessToSem(new_sem, pid);
    }

    Sem_Node_t *new_node = (Sem_Node_t *)malloc(sizeof(Sem_Node_t));
    if (new_node == NULL)
    {
        free((void *)new_sem);
        return NULL;
    }
    new_node->semaphore = new_sem;
    new_node->next = NULL;

    return new_node;
}

void semClose(uint64_t id, uint64_t pid, uint64_t *resp)
{
    Sem_Node_t *iterator = first;
    Sem_Node_t *prev = NULL;

    while (iterator != NULL)
    {
        if (iterator->semaphore->id == id)
        {
            removeProcessFromSem(iterator->semaphore, pid);
            if (iterator->semaphore->processes == NULL)
            {
                if (prev == NULL)
                {
                    first = iterator->next;
                }
                else
                {
                    prev->next = iterator->next;
                }
                free((void *)iterator->semaphore);
                free((void *)iterator);
                *resp = 2;
                return;
            }
            *resp = 0;
            return;
        }
        prev = iterator;
        iterator = iterator->next;
    }
    *resp = 1;
    semPrintAll();
    return;
}

void removeProcessFromSem(Semaphore_t *sem, uint64_t pid)
{
    Process_Node_t *prev = NULL;
    Process_Node_t *iterator = sem->processes;

    while (iterator != NULL)
    {
        if (iterator->pid == pid)
        {
            if (prev == NULL)
            {
                if (iterator->next != NULL)
                {
                    sem->processes = iterator->next;
                }
                else
                {
                    sem->processes = NULL;
                }
            }
            else
            {
                prev->next = iterator->next;
            }
            free(iterator);
            return;
        }
        prev = iterator;
        iterator = iterator->next;
    }
    return;
}

static void sleep(Process_Node_t *proc)
{
    proc->is_blocked = 1;
    lockToSem(proc->pid);
    yieldProcess();
}

void semWait(uint64_t id, uint64_t pid, uint64_t *resp)
{
    Semaphore_t *sem = getSem(id);

    if (sem == NULL)
    {
        put_string("Semaphore not found\n", C_WHITE);
        *resp = -1;
        return;
    }

    Process_Node_t *proc = getProcess(sem, pid);

    if (proc == NULL)
    {
        put_string("Process not found\n", C_WHITE);
        *resp = -2;
        return;
    }

    int turn = 0;
    while (turn == 0)
    {
        spinLock(&(sem->lock));
        if (sem->count > 0)
        {
            sem->count--;
            turn = 1;
        }
        else
        {
            spinUnlock(&(sem->lock));
            sleep(proc);
        }
    }
    *resp = 0;
    spinUnlock(&(sem->lock));

    return;
}
void semPost(uint64_t id, uint64_t *resp)
{
    Semaphore_t *sem = getSem(id);

    if (sem == NULL)
    {
        put_string("No semaphore found \n", C_WHITE);
        *resp = -1;
        //semPrintAll();
        return;
    }

    spinLock(&(sem->lock));
    sem->count++;

    unlockFirstBlockedProcess(sem);
    *resp = 0;
    spinUnlock(&(sem->lock));

    return;
}
Semaphore_t *getSem(uint64_t id)
{

    Sem_Node_t *iterator = first;
    while (iterator != NULL)
    {

        if (iterator->semaphore->id == id)
        {
            return iterator->semaphore;
        }
        iterator = iterator->next;
    }
    return NULL;
}

Process_Node_t *getProcess(Semaphore_t *sem, uint64_t pid)
{
    Process_Node_t *proc_iterator = sem->processes;
    while (proc_iterator != NULL)
    {

        if (proc_iterator->pid == pid)
        {
            return proc_iterator;
        }
        proc_iterator = proc_iterator->next;
    }
    return NULL;
}

void unlockFirstBlockedProcess(Semaphore_t *sem)
{

    Process_Node_t *iterator = sem->processes;
    while (iterator != NULL)
    {
        if (iterator->is_blocked == 1)
        {

            iterator->is_blocked = 0;
            unlockFromSem(iterator->pid);
            return;
        }
        iterator = iterator->next;
    }
}

void semPrintAll()
{
    if (first == NULL)
    {
        put_string("No semaphores found\n", C_WHITE);
        return;
    }
    put_string("Semaphores: \n", C_WHITE);

    Sem_Node_t *iterator = first;
    while (iterator != NULL)
    {
        printSem(iterator->semaphore);
        iterator = iterator->next;
    }
    return;
}

void printSem(Semaphore_t *sem)
{
    put_string("name: ", C_WHITE);
    if (sem->name != NULL)
    {
        put_string(sem->name, C_WHITE);
    }
    else
    {
        put_string("NOT_SPECIFIED", C_WHITE);
    }
    put_string(" | ", C_WHITE);
    put_string("id: ", C_WHITE);
    put_dec(sem->id, C_WHITE);
    put_string(" | ", C_WHITE);
    put_string("count: ", C_WHITE);
    put_dec(sem->count, C_WHITE);
    put_string(" | ", C_WHITE);
    put_string("processes: ", C_WHITE);
    printSemProcesses(sem->processes);
    put_string("\n", C_WHITE);
    return;
}

void printSemProcesses(Process_Node_t *process)
{
    if (process == NULL)
    {
        put_string("NOT_FOUND", C_WHITE);
    }
    while (process != NULL)
    {
        put_string("(pid: ", C_WHITE);
        put_dec(process->pid, C_WHITE);
        put_string(", blocked: ", C_WHITE);
        char *blocked = (process->is_blocked == 1) ? "yes" : "no";

        put_string(blocked, C_WHITE);
        put_string(")", C_WHITE);
        process = process->next;
    }
}

int strcmp(char *s1, char *s2)
{
    unsigned char c1, c2;
    while ((c1 = *s1++) == (c2 = *s2++))
    {
        if (c1 == '\0')
            return 0;
    }
    return c1 - c2;
}