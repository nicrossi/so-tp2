// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <video_driver.h>
#include <time.h>
#include <sound_driver.h>
#include <heap_interface.h>
#include <stddef.h>
#include <scheduler.h>

#include <pipes.h>
#include <semaphore.h>
#include <screen.h>

typedef uint64_t (*SystemCall)();
//video_driver.h
static uint64_t __get_width();
static uint64_t __get_height();
static void __clear_screen();

//sound_driver.h
static void __beep(int ticks);

//time.h
static void __wait(int ticks);
static int __get_hours();
static int __get_minutes();
static int __get_seconds();
static int __ticks_elapsed();

//heap.h
static void *__malloc(size_t size);
static void __free(void *ptr);
static void __getHeapStats(HeapStats_t *stats);

//scheduler.h
static void __ps();
static pid_t __create_process(char *name, int argc, char **argv, void *entryPoint, uint64_t *parameters);
static int __kill_process(pid_t pid);
static int __nice(pid_t pid, uint64_t newPriority);
static int __mem(const unsigned char *ptr);
static int __block(pid_t pid);
static pid_t __getPID();

//pipes.h
static uint64_t __open_pipe();
static uint64_t __close_pipe(uint64_t id);
static void __print_pipes();
static uint64_t __read_pipe(char *addr, int64_t id);
static uint64_t __write_pipe(char *addr, int64_t id);
static uint64_t __read_std(char *addr);
static uint64_t __write_std(char *addr);
//semaphore.h
static void __sys_semaphore(void *option, void *arg1, void *arg2, void *arg3, void *arg4);
static void __yield();
SystemCall syscall_array[] = {
    0,
    0,
    0,
    (SystemCall)__get_width,
    (SystemCall)__get_height,
    (SystemCall)__clear_screen,
    (SystemCall)__beep,
    (SystemCall)__wait,
    (SystemCall)__get_hours,
    (SystemCall)__get_minutes,
    (SystemCall)__get_seconds,
    (SystemCall)__ticks_elapsed,
    (SystemCall)__malloc,
    (SystemCall)__free,
    (SystemCall)__getHeapStats,
    (SystemCall)__create_process,
    (SystemCall)__kill_process,
    (SystemCall)__ps,
    (SystemCall)__nice,
    (SystemCall)__mem,
    (SystemCall)__block,
    (SystemCall)__getPID,
    (SystemCall)__sys_semaphore,
    (SystemCall)__open_pipe,
    (SystemCall)__close_pipe,
    (SystemCall)__print_pipes,
    (SystemCall)__read_pipe,
    (SystemCall)__write_pipe,
    (SystemCall)__read_std,
    (SystemCall)__write_std,
    (SystemCall)__yield};

void syscallDispatcher(uint64_t index, uint64_t a, uint64_t b, uint64_t c, uint64_t d, uint64_t e)
{

    syscall_array[index](a, b, c, d, e);
}

static void __sys_semaphore(void *option, void *arg1, void *arg2, void *arg3, void *arg4)
{
    sys_semaphore(option, arg1, arg2, arg3, arg4);
}

static uint64_t __read_pipe(char *addr, int64_t id)
{
    return readPipe(addr, id);
}
static uint64_t __write_pipe(char *addr, int64_t id)
{
    return writePipe(addr, id);
}

static uint64_t __read_std(char *addr)
{
    return readStd(addr);
}
static uint64_t __write_std(char *addr)
{
    return writeStd(addr);
}

static uint64_t __open_pipe()
{
    return openPipe();
}

static uint64_t __close_pipe(uint64_t id)
{
    return closePipe(id);
}
static void __print_pipes()
{
    printPipes();
}

static void __getHeapStats(HeapStats_t *stats)
{
    getHeapStats(stats);
}

static void __free(void *ptr)
{
    free(ptr);
}
static void *__malloc(size_t size)
{
    return malloc(size);
}

static uint64_t __get_width()
{
    return get_width();
}

static uint64_t __get_height()
{
    return get_height();
}

static void __clear_screen()
{
    clear_screen();
}

static void __beep(int ticks)
{
    beep(ticks);
}

static void __wait(int ticks)
{
    return wait(ticks);
}

static int __get_hours()
{
    return get_hours();
}

static int __get_minutes()
{
    return get_minutes();
}

static int __get_seconds()
{
    return get_seconds();
}

static int __ticks_elapsed()
{
    return ticks_elapsed();
}

static pid_t __create_process(char *name, int argc, char **argv, void *entryPoint, uint64_t *parameters)
{

    return createProcess(name, argc, argv, entryPoint, READY, parameters);
}

static int __kill_process(pid_t pid)
{
    return killProcessByPID(pid);
}

static pid_t __getPID()
{
    return getPID();
}

static void __ps()
{
    ps();
}

static int __nice(pid_t pid, uint64_t newPriority)
{
    return changePriority(pid, newPriority);
}

static int __mem(const unsigned char *ptr)
{

    for (int i = 0; i < 256; i++)
    {
        unsigned char digit1 = *(ptr + i) / 16;
        unsigned char digit2 = *(ptr + i) % 16;
        put_int(digit1, 16, C_WHITE);
        put_int(digit2, 16, C_WHITE);
        //printHex(digit2);
        put_string(" ", C_WHITE);
    }

    return 0;
}

static int __block(pid_t pid)
{
    ProcessQueueNode_t *aux = getProcessByPid(pid);
    if (aux->pcb.state == READY)
    {

        changeState(pid, BLOCKED);

        return 0;
    }
    else if (aux->pcb.state == BLOCKED)
    {
        changeState(pid, READY);

        return 0;
    }
    return 0;
}

static void __yield()
{
    yieldProcess();
}