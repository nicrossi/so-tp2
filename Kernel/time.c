// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <time.h>
#include <scheduler.h>
#include <interrupts.h>

static unsigned long ticks = 0;

void timer_handler()
{
	ticks++;
	//aca en base a lo de rr habria que hacer el cambio
	//checkIfChange();
}

int ticks_elapsed()
{
	return ticks;
}

int get_minutes()
{
	return _get_minutes();
}

int get_hours()
{
	return _get_hours();
}

int get_seconds()
{
	return _get_seconds();
}

void wait(int ticks)
{
	_sti();
	int start = ticks_elapsed();
	int end = ticks + start;
	while (ticks_elapsed() < end)
		;
}