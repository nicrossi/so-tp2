# x64BareBones 

x64BareBones is a basic setup to develop operating systems for the Intel 64 bits architecture.

The final goal of the project is to provide an entry point for a kernel and the possibility to load extra binary modules separated from the main kernel.

## Environment setup

Install the following packages before building the Toolchain and Kernel:

- nasm
- qemu
- gcc
- make

## Usage

1. Build the Toolchain

Execute the following commands on the x64BareBones project directory:

```bash  
cd Toolchain
make all
```

2. Build the Kernel

From the x64BareBones project directory run:

```bash
make all
```

3. Run the kernel

From the x64BareBones project directory run:

```bash
./run.sh
```

# Docker

You can also run x64BareBones in a Docker image by following these instructions.

## Prerequisites 

You will need to have a Docker account and install the following packages

- docker
- qemu

Make sure that the following files are in your project directory: 

- Dockerfile
- docker.sh

# Usage

1. In your terminal, from your main project directory. Run the command to compile and build the project.

```bash
./docker.sh
```

2. Then run the project with the command:
```bash
./run.sh
```


