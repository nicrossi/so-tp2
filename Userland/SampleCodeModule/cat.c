// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <cat.h>
#include <stdio.h>
#define ESC 27
void cat()
{
    char c;
    while ((c = getchar()) != ESC)
    {
        putchar(c);
    }
}