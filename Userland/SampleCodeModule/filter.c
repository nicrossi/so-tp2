// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <filter.h>
#include <stdio.h>
#define ESC 27
void filter()
{
    char c;

    while ((c = getchar()) != ESC)
    {
        if (c != 'a' && c != 'e' && c != 'i' && c != 'o' && c != 'u' &&
            c != 'A' && c != 'E' && c != 'I' && c != 'O' && c != 'U')
        {
            putchar(c);
        }
    }
}