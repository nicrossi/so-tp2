// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "help.h"
#include "syscall.h"
#include "stdio.h"

static void start_help();

void help()
{
    //    char *arg = "help";
    //    char ** argv;
    //    *argv[0] = arg;

    /*__syscall(__createProcess, 1, argv, ?????)*/

    start_help();
}

static void start_help()
{
    printf("Commands:\n");
    printf("\thelp -- Shows available commands.\n");
    printf("\tclear -- Clears the screen.\n");
    printf("\tbeep @num -- Receives an number as a parameter and beeps for that amount of ticks.\n");
    printf("\techo @string -- Recieves a string as parameter and repeats it as output\n");
    printf("\twidth -- Shows the screen width.\n");
    printf("\theight -- Shows the screen height.\n");
    printf("\tdiv0 -- Tests division by zero exception.\n");
    printf("\tinvOp -- Tests invalid op code exception.\n");
    printf("\ttime -- Shows the current system time.\n");
    printf("\tkill @pid -- Print all processes.\n");
    printf("\tps -- Print all processes.\n");
    printf("\tnice @pid @new_priority -- Change priority of process pid.\n");
    printf("\tmem @address -- Prints the value of 32 bytes of memory starting from @address.\n");
    printf("\thstats -- Prints the heap stats.\n");
    printf("\tblock @pid -- Locks a process (if ready) and unlocks (if blocked).\n");
    printf("\tloop -- Create process loop, a process that prints every 2 seconds.\n");
    printf("\tloop2 -- Create process loop2, useful for priority testing.\n");
    printf("\ttest_mm -- Run memory managment test.\n");
    printf("\ttest_proc -- Run process creation test.\n");
    printf("\ttest_prio -- Run process priority test.\n");
    printf("\ttest_sync -- Run processes with synchronization test.\n");
    printf("\ttest_no_sync -- Run processes without synchronization test.\n");
    printf("\tphilo -- Create process loop.\n");
    printf("\tcat -- Prints the stdin as received.\n");
    printf("\twc -- Counts the number of input lines.\n");
    printf("\tfilter -- Filters the input vowels.\n");
}