#ifndef _PHILOSOPHERS_H
#define _PHILOSOPHERS_H

#include <sem_module.h>
#include "stdio.h"
#include <stdlib.h>
#include <ps_module.h>
#include <time_module.h>
#include <sys/types.h>

#define INITIAL_PHILOS_QUANTITY 5

#define MAX_DIGIT 11

typedef enum philoState
{
    THINKING = 0,
    HUNGRY,
    EATING
} philoState;

typedef struct Fork_t
{
    Sem_Info_t *fork_id;
    char fork_name[MAX_DIGIT];
} Fork_t;

typedef struct Philosopher_t
{
    pid_t pid;

    philoState s;
    Fork_t *left_fork;
    Fork_t *right_fork;
    Sem_Info_t *philo_sem;
    char philo_name[MAX_DIGIT];
    int active_duty;
} Philosopher_t;

typedef struct Philo_Node_t
{
    Philosopher_t *philo;
    struct Philo_Node_t *prev;
    struct Philo_Node_t *next;
} Philo_Node_t;

void philosophers_process();
void run_philosophers();
void add_philosopher();
void remove_philosopher();
void print_philosophers();
Philosopher_t *createPhilosopher();
Fork_t *createFork();
Philosopher_t *getLastPhilosopher();
void takeForks(Sem_Info_t *small, Sem_Info_t *big);
void leaveForks(Sem_Info_t *small, Sem_Info_t *big);
Sem_Info_t *getSmallestFork(Philosopher_t *philo);
Sem_Info_t *getBiggestFork(Philosopher_t *philo);

#endif