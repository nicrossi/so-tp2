#ifndef PIPE_MODULE_H
#define PIPE_MODULE_H
#include <syscall.h>
#include <sys/types.h>

uint64_t pipe();
uint64_t closePipe(uint64_t id);
uint64_t readPipe(char *addr, uint64_t id);
uint64_t writePipe(char *addr, uint64_t id);
void printPipes();

#endif