#ifndef PS_MODULE_H
#define PS_MODULE_H
#include <syscall.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdlib.h>

void ps();
pid_t getPID();
int kill_process(pid_t pid);
pid_t create_process(char *name, int argc, char *argv[], void *entryPoint, uint64_t *parameters);
int nice(pid_t pid, uint64_t newPriority);
int printmem(uint64_t address);
int block();

#endif //SO_TP2_PS_MODULE_H
