#ifndef SEM_MODULE_H
#define SEM_MODULE_H
#include <syscall.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdlib.h>

enum semState
{
    OPEN,
    CLOSE,

    WAIT,
    POST,
    PRINTALL
};

typedef struct Sem_Info_t
{
    uint64_t id;
} Sem_Info_t;
Sem_Info_t *semOpen(char *name, uint64_t value);
int semClose(Sem_Info_t *sem_info);

int semWait(Sem_Info_t *sem_info);
int semPost(Sem_Info_t *sem_info);
void printAllSemInfo();

#endif