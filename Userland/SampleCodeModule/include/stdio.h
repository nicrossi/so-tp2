#ifndef STDIO_H
#define STDIO_H
#include <stdint.h>

#define STD_OUT 0
#define STD_IN 0
#define STD_ERR 1

void printf(char *format, ...);

void putchar(char c);
char getchar();
void perror(char *s);
void putint(int64_t value, int base);
uint64_t writeStd(char *buffer, uint64_t size);
uint64_t readStd(char *buffer, uint64_t size);

char getchar_from_buffer(int buffer);

#endif
