#ifndef STDLIB_H
#define STDLIB_H
#include <stddef.h>
#include <stdint.h>

typedef struct HeapStats
{
    size_t totalHeapSizeInBytes;           /*The total heap size*/
    size_t occupiedHeapSpaceInBytes;       /* The total heap size currently occupied*/
    size_t availableHeapSpaceInBytes;      /* The total heap size currently available - this is the sum of all the free blocks, not the largest block that can be allocated. */
    size_t sizeOfLargestFreeBlockInBytes;  /* The maximum size, in bytes, of all the free blocks within the heap at the time vPortGetHeapStats() is called. */
    size_t sizeOfSmallestFreeBlockInBytes; /* The minimum size, in bytes, of all the free blocks within the heap at the time vPortGetHeapStats() is called. */
    size_t numberOfFreeBlocks;             /* The number of free memory blocks within the heap at the time vPortGetHeapStats() is called. */
    size_t numberOfSuccessfulAllocations;  /* The number of calls to pvPortMalloc() that have returned a valid memory block. */
    size_t numberOfSuccessfulFrees;        /* The number of calls to vPortFree() that has successfully freed a block of memory. */
} HeapStats_t;
// Compares two strings and returns 1 if they're equal.
int strcmp(char *s1, char *s2);
//recieves an int and converts it to a char* using the given base.
char *itoa(int value, int base, char *buffer);
void reverse(char *s, int size);
int strlen(char *s);
int atoi(char *str);
int isdigit(char c);
void *malloc(size_t size);
void free(void *ptr);
void getHeapStats(HeapStats_t *stats);
void printHeapStats();
void strcpy(char *src, char *dest);
char *strtok(char *s, char *token);
char *strcat(char *s1, const char *s2);
uint32_t uintToBase(uint64_t value, char *buffer, uint32_t base);
#endif
