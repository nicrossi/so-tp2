#ifndef SYSTEMCALL_H
#define SYSTEMCALL_H
#include <stdint.h>

#define __get_width 3
#define __get_height 4

#define __clear_screen 5

#define __beep 6
#define __wait 7
#define __get_hours 8
#define __get_minutes 9
#define __get_seconds 10
#define __ticks_elapsed 11
#define __malloc 12
#define __free 13
#define __heapStatus 14
#define __create_process 15
#define __kill_process 16
#define __ps 17
#define __nice 18
#define __mem 19
#define __block 20
#define __getPID 21
#define __sys_semaphore 22
#define __open_pipe 23
#define __close_pipe 24
#define __print_pipes 25
#define __read_pipe 26
#define __write_pipe 27
#define __read_std 28
#define __write_std 29
#define __yield 30

uint64_t _syscall(uint64_t arg1, ...);

#endif
