#include <stdint.h>

uint32_t GetUint();
uint32_t GetUniform(uint32_t max);
uint8_t memcheck(void *start, uint8_t value, uint32_t size);
void test_memset(void *start, uint8_t value, uint32_t size);
uint32_t my_create_process(char *name, void *entryPoint);

uint32_t my_kill(uint32_t pid);

uint32_t my_block(uint32_t pid);

uint64_t my_nice(uint64_t pid, uint64_t newPrio);