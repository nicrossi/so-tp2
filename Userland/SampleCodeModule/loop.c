// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <loop.h>
#include <ps_module.h>
#include <time_module.h>
#include <stdio.h>
void loop()
{
    int last = ticks_elapsed() / 18;
    pid_t pid = getPID();
    
    while (1)
    {

        int now = ticks_elapsed() / 18;
        if (now >= last + 2)
        {
            printf("\nLoop: Hello from process with PID: %d\n", pid);

            last = now;
        }
    }

    return;
}