// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <loop2.h>
#include <ps_module.h>
#include <sys/types.h>
#include <stdio.h>
#define SEM_ID "loop2"

void loop2()
{
    pid_t pid = getPID();
    uint64_t i = 0;
    while (1)
    {

        if (i % 500 == 0)
        {
            printf("\nLoop 2: Hello from process with PID: %d\n", pid);
        }
        i++;
    }
}