// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <philosophers.h>
#include <video_module.h>

#include <stdlib.h>

uint64_t philo_counter = 0;
uint64_t fork_counter = 0;
Philo_Node_t *first = NULL;
Philosopher_t *last_philosopher = NULL;

#define MAX_SIZE 255
#define ESC 27
#define BACKSPACE '\b'
#define SPACE ' '
#define BG 1
#define FG 0

static const Color BLACK = {0, 0, 0};
static const Color RED = {255, 0, 0};
static const Color GREEN = {0, 255, 0};
static const Color BLUE = {0, 0, 255};
static const Color WHITE = {255, 255, 255};

static char *starting_tag = "~>";

static int keeps_going = 1;
void do_the_loop();
void philo_refresh_stdin();

void run_philosophers()
{
  keeps_going = 1;

  philo_counter = 0;
  fork_counter = 0;
  clear_screen();
  printf("=~= ---------------Welcome to philosophers-------------- =~=\n\n");
  printf("=~= Press 'a' to add a philosopher and 'r' to remove one =~=\n\n");
  printf("=~= ---------Press ESC or Enter to exit simulation------ =~=\n\n");

  do_the_loop();
  return;
}

void add_philosopher()
{
  printf("Adding philosopher\n");

  Philosopher_t *new_philo = createPhilosopher();

  Philo_Node_t *aux = (Philo_Node_t *)malloc(sizeof(Philo_Node_t));
  if(aux == NULL)
  {
    printf("ERROR allocating memory\n");
    return;
  }
  aux->philo = new_philo;

  if (philo_counter == 0)
  {
    new_philo->left_fork = createFork();
    new_philo->right_fork = createFork();

    aux->next = aux;
    aux->prev = aux;
    first = aux;
  }
  else if (philo_counter == 1)
  {
    first->next = aux;
    first->prev = aux;
    aux->next = first;
    aux->prev = first;
    aux->philo->left_fork = first->philo->right_fork;
    aux->philo->right_fork = first->philo->left_fork;
  }
  else
  {
    Fork_t *new_fork = createFork();
    new_philo->left_fork = new_fork;

    semWait(first->prev->philo->philo_sem);

    semWait(first->philo->left_fork->fork_id);

    new_philo->right_fork = first->philo->left_fork;
    first->prev->philo->right_fork = new_fork;

    first->prev->next = aux;
    aux->prev = first->prev;
    first->prev = aux;
    aux->next = first;

    semPost(first->philo->left_fork->fork_id);
    semPost(first->prev->prev->philo->philo_sem);
  }
  last_philosopher = new_philo;
  philo_counter++;

  void (*func)(void);
  func = &philosophers_process;
  uint64_t parameters[] = {13, 1, 0, 0};
  char *argv[1];
  argv[0] = "philo";
  new_philo->pid = create_process("philo", 1, argv, func, parameters);
  return;
}

Philosopher_t *createPhilosopher()
{
  Philosopher_t *new_philo = (Philosopher_t *)malloc(sizeof(Philosopher_t));
  if (new_philo == NULL)
  {
    printf("ERROR creating philosopher\n");
    return NULL;
  }

  new_philo->active_duty = 1;
  new_philo->s = THINKING;
  uintToBase(philo_counter, new_philo->philo_name, 10);
  char *aux_name = "_philo_sem";
  strcat(new_philo->philo_name, aux_name);
  new_philo->philo_sem = semOpen(new_philo->philo_name, 1);
  return new_philo;
}

Fork_t *createFork()
{
  Fork_t *new_fork = (Fork_t *)malloc(sizeof(Fork_t));
  if (new_fork == NULL)
  {
    printf("ERROR creating fork\n");
    return NULL;
  }
  uintToBase(fork_counter, new_fork->fork_name, 10);
  char *aux_name = "fork_sem";
  strcat(new_fork->fork_name, aux_name);
  Sem_Info_t *sem = semOpen(new_fork->fork_name, 1);
  new_fork->fork_id = sem;
  fork_counter++;
  return new_fork;
}

void remove_philosopher()
{
  if (philo_counter <= 0)
  {
    perror("You can't have less than 0 philosophers\n");
    return;
  }
  printf("Removing philosopher\n");

  philo_counter--;

  if (philo_counter == 0)
  {
    first->philo->active_duty = 0;
    semWait(first->philo->philo_sem);
    semPost(first->philo->philo_sem);

    semClose(first->philo->philo_sem);
    semClose(first->philo->left_fork->fork_id);
    semClose(first->philo->right_fork->fork_id);
    free(first->philo->right_fork);
    free(first->philo->left_fork);
    free(first);
    fork_counter -= 2;
  }
  else if (philo_counter == 1)
  {
    first->prev->philo->active_duty = 0;
    semWait(first->prev->philo->philo_sem);
    semPost(first->prev->philo->philo_sem);

    semClose(first->prev->philo->philo_sem);
    free(first->prev);
  }
  else if (philo_counter >= 2)
  {
    semWait(first->prev->prev->philo->philo_sem);
    first->prev->philo->active_duty = 0;
    semWait(first->prev->philo->philo_sem);
    semPost(first->prev->philo->philo_sem);
    semClose(first->prev->philo->left_fork->fork_id);
    free(first->prev->philo->left_fork);

    Philo_Node_t *aux_to_del = first->prev;
    first->prev = first->prev->prev;
    first->prev->next = first;

    first->prev->philo->right_fork = first->philo->left_fork;

    semClose(aux_to_del->philo->philo_sem);
    free(aux_to_del->philo);
    free(aux_to_del);

    semPost(first->prev->philo->philo_sem);

    fork_counter--;
  }
}

void print_philosophers()
{
  if (first == NULL)
  {
    perror("No philos\n");
    return;
  }
  Philo_Node_t *iterator = first;
  for (int i = 0; i < philo_counter; i++, iterator = iterator->next)
  {
    switch (iterator->philo->s)
    {
    case EATING:
      printf("E");
      break;
    default:
      printf(".");
      break;
    }
  }
  printf("\n\n");
}

Philosopher_t *getLastPhilosopher()
{
  return last_philosopher;
}

void philosophers_process()
{
  Philosopher_t *philo = getLastPhilosopher();
  semOpen(philo->philo_name, 1);
  while (philo->active_duty)
  {
    philo->s = THINKING;
    print_philosophers();
    wait(36);

    philo->s = HUNGRY;
    print_philosophers();

    semWait(philo->philo_sem);
    if (philo->active_duty)
    {
      semOpen(philo->left_fork->fork_name, 1);
      semOpen(philo->right_fork->fork_name, 1);
      Sem_Info_t *small = getSmallestFork(philo);
      Sem_Info_t *big = getBiggestFork(philo);
      takeForks(small, big);
      philo->s = EATING;
      print_philosophers();
      wait(36);
      leaveForks(small, big);
      semClose(philo->left_fork->fork_id);
      semClose(philo->right_fork->fork_id);
    }
    semPost(philo->philo_sem);
  }
  semClose(philo->philo_sem);
}

void takeForks(Sem_Info_t *small, Sem_Info_t *big)
{
  semWait(small);
  semWait(big);
}

void leaveForks(Sem_Info_t *small, Sem_Info_t *big)
{
  semPost(small);
  semPost(big);
}

Sem_Info_t *getSmallestFork(Philosopher_t *philo)
{
  return (philo->left_fork->fork_id->id < philo->right_fork->fork_id->id) ? philo->left_fork->fork_id : philo->right_fork->fork_id;
}

Sem_Info_t *getBiggestFork(Philosopher_t *philo)
{
  return (philo->left_fork->fork_id->id < philo->right_fork->fork_id->id) ? philo->right_fork->fork_id : philo->left_fork->fork_id;
}
//--------------------------------------FUNCIONES DEL LOOP DEL INPUT-----------------------------------------------------------------------------
void do_the_loop()
{
  while (keeps_going)
  {
    philo_refresh_stdin();
  }
  while (philo_counter > 0)
  {
    remove_philosopher();
  }
  printf("Returning to shell...\n\n");
  printf("Welcome to shell!\n");
  printf("Write 'help' to see a list of commands!\n");
  printf("%s", starting_tag);
}

void philo_refresh_stdin()
{

  char c;

  while ((c = getchar()) != '\n' && c != ESC)
  {

    if (c == 'a' || c == 'A')
    {
      add_philosopher();
    }
    else if (c == 'r' || c == 'R')
    {
      remove_philosopher();
    }
  }

  putchar('\n');
  keeps_going = 0;
}
