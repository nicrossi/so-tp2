// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <pipe_module.h>

uint64_t pipe()
{
    return _syscall(__open_pipe);
}
uint64_t closePipe(uint64_t id)
{
    return _syscall(__close_pipe, id);
}
void printPipes()
{
    _syscall(__print_pipes);
}

uint64_t readPipe(char *addr, uint64_t id)
{
    return _syscall(__read_pipe, addr, id);
}
uint64_t writePipe(char *addr, uint64_t id)
{
    return _syscall(__write_pipe, addr, id);
}

uint64_t writeStd(char *buffer, uint64_t size)
{

    int i = 0;
    int finished = 0;
    char c = 0;
    while (i < size && !finished)
    {
        c = buffer[i++];
        _syscall(__write_std, &c);

        if (c == '\0')
        {
            finished = 1;
        }
    }

    return i;
}
uint64_t readStd(char *buffer, uint64_t size)
{

    int i = 0;
    int finished = 0;
    while (i < size && !finished)
    { // Mientras no se llene el buffer
        char c;

        _syscall(__read_std, &c);

        buffer[i++] = c;
        if (c == '\0')
        {
            i--;
            finished = 1;
        }
    }
    return i;
}