// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "ps_module.h"
#include <stdio.h>
void ps()
{
    _syscall(__ps);
}
pid_t getPID()
{
    return _syscall(__getPID);
}

int kill_process(pid_t pid)
{
    return _syscall(__kill_process, pid);
}

pid_t create_process(char *name, int argc, char *argv[], void *entryPoint, uint64_t *parameters)
{

    return _syscall(__create_process, name, argc, argv, entryPoint, parameters);
}

int nice(pid_t pid, uint64_t newPriority)
{
    return _syscall(__nice, pid, newPriority);
}

int printmem(uint64_t address)
{
    return _syscall(__mem, address);
}

int block(pid_t pid)
{
    return _syscall(__block, pid);
}
