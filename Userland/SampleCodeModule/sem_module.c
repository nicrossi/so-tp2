// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "sem_module.h"
#include <stdio.h>
#include "ps_module.h"
Sem_Info_t *semOpen(char *name, uint64_t value)
{
    int pid = getPID();
    Sem_Info_t *aux = (Sem_Info_t *)malloc(sizeof(Sem_Info_t));
    if(aux == NULL)
    {
        printf("ERROR creating semaphores\n");
        return NULL;
    }
    uint64_t resp;
    _syscall(__sys_semaphore, OPEN, name, pid, value, &resp);
    aux->id = resp;
    return aux;
}

int semClose(Sem_Info_t *sem_info)
{
    int pid = getPID();
    uint64_t id = sem_info->id;
    int resp;
    _syscall(__sys_semaphore, CLOSE, id, pid, &resp);
    if (resp == 1)
    {
        printf("No semaphore %d was found\n", (int32_t)id);
    }
    else if (resp == 2)
    {
        free(sem_info);
        //printf("Semaphore closed succesfully\n");
    }
    return resp;
}

int semWait(Sem_Info_t *sem_info)
{
    int pid = getPID();
    uint64_t id = sem_info->id;
    // printf("sem wait: %d\n", sem_info->id);
    int resp;
    _syscall(__sys_semaphore, WAIT, id, pid, &resp);
    if (resp == -1)
    {
        printf("Wait: No semaphore %d was found\n", (int32_t)id);
    }
    return resp;
}

int semPost(Sem_Info_t *sem_info)
{
    int resp;
    uint64_t id = sem_info->id;

    _syscall(__sys_semaphore, POST, id, &resp);
    if (resp == 1)
    {
        printf("Post: No semaphore %d was found \n", (int32_t)id);
    }
    if (resp == 2)
    {
        printf("FATAL ERROR %d", (int32_t)id);
    }
    return resp;
}

void printAllSemInfo()
{
    _syscall(__sys_semaphore, PRINTALL);
}
