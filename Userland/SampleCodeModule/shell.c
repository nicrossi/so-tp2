// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <shell.h>
#include <video_module.h>
#include <sound_module.h>
#include <time_module.h>
#include <syscall.h>

#include <stdio.h>
#include <stdlib.h>

#include <exception_tester.h>
#include <ps_module.h>
#include <sys/types.h>
#include <help.h>
#include <loop.h>
#include <cat.h>
#include <filter.h>
#include <wc.h>
#include "test_mm.h"
#include "test_util.h"
#include "test_processes.h"
#include "test_prio.h"
#include "pipe_module.h"
#include "test_sync.h"
#include "sem_module.h"
#include "philosophers.h"
#include "loop2.h"
#define STDOUT 1
#define STDERR 2
#define MAX_SIZE 255
#define ESC 27
#define BACKSPACE '\b'
#define SPACE ' '
#define COMMANDS_SIZE 10
#define BG 1
#define FG 0

static const Color BLACK = {0, 0, 0};
static const Color RED = {255, 0, 0};
static const Color GREEN = {0, 255, 0};
static const Color BLUE = {0, 0, 255};
static const Color WHITE = {255, 255, 255};

static int buffer_index = 0;

static char *starting_tag = "~>";
static char buffer[20000];

void init();
void consoleLoop();
void refresh_stdin();

static void clear_buffer();

static void display_time();
static void display_screen_width();
static void display_screen_height();

static void mem(uint64_t addr);
static void div0();
static void invOp();
static void run_command(char *name, void (*entryPoint)(void), int type, int stdin, int stdout);
static void parse_parameters(char *src, char *dest[COMMANDS_SIZE]);
static void command_dispatcher(char *buffer);

static char *
    runnable_commands_names[] = {
        "loop",
        "loop2",
        "test_mm",
        "test_proc",
        "test_prio",
        "test_sync",
        "test_no_sync",
        "philo",
        "cat",
        "wc",
        "filter"};
static void (*runnable_commands[])(void) = {loop, loop2, test_mm, test_processes, test_prio, test_sync, test_no_sync, run_philosophers, cat, wc, filter};

static void run_command(char *name, void (*entryPoint)(void), int type, int stdin, int stdout)
{

  char *argv[1];
  argv[0] = name;
  uint64_t parameters[] = {6, type, stdin, stdout};

  create_process(name, 1, argv, entryPoint, parameters);
}

void start_shell()
{
  init();
  consoleLoop();
}

void init()
{
  printf("Welcome to shell!\n");
  printf("Write 'help' to see a list of commands!\n");
}

void consoleLoop()
{

  while (1)
  {
    refresh_stdin();
  }
}

void refresh_stdin()
{
  buffer_index = 0;
  char c;

  printf("%s", starting_tag);
  while ((c = getchar()) != '\n')
  {
    if (c == BACKSPACE)
    {
      if (buffer_index > 0)
      {
        buffer[buffer_index] = 0;
        buffer_index--;
        putchar(c);
      }
    }
    else
    {
      buffer[buffer_index++] = c;
      putchar(c);
    }
  }
  buffer[buffer_index] = '\0';
  putchar('\n');
  //new_line();
  command_dispatcher(buffer);
  clear_buffer();
}

static void clear_buffer()
{
  for (int i = 0; i < buffer_index; i++)
    buffer[i] = 0;
  buffer_index = 0;
}

static void command_dispatcher(char *buffer)
{
  int i, j, k, l;
  char command[MAX_SIZE] = {0};
  char parameter[MAX_SIZE] = {0};
  char *params[14];
  int reading_command = 1;
  int pipe_requested = 0;
  uint64_t pipe_id;

  for (i = 0, j = 0; i < MAX_SIZE - 1 && buffer[i] != 0 && j < MAX_SIZE - 1; i++)
  {
    if (buffer[i] == SPACE && reading_command)
    {
      reading_command = 0;
      command[i] = 0;
    }
    else if (reading_command)
      command[i] = buffer[i];

    else
      parameter[j++] = buffer[i];
  }
  parse_parameters(parameter, params);
  int type = FG;
  for (k = 0; k < COMMANDS_SIZE; k++)
  {
    if (strcmp(command, runnable_commands_names[k]) == 0)
    {
      if (params[0] != NULL)
      {
        if (strcmp(params[0], "&") == 0)
        {
          type = BG;
        }
        else if (strcmp(params[0], "|") == 0)
        {
          for (l = 0; l < COMMANDS_SIZE; l++)
          {
            if (strcmp(params[1], runnable_commands_names[l]) == 0)
            {
              pipe_requested = 1;
              break;
            }
          }
        }
        if (pipe_requested)
        {
          pipe_id = pipe();
          run_command(runnable_commands_names[k], runnable_commands[k], type, 0, pipe_id);

          run_command(runnable_commands_names[l], runnable_commands[l], type, pipe_id, 0);

          pipe_requested = 0;
          closePipe(pipe_id);
          return;
        }
        else
        {
          run_command(runnable_commands_names[k], runnable_commands[k], type, 0, 0);
          return;
        }
      }
    }
  }

  if (strcmp(command, "help") == 0)
  {
    help();
  }
  else if (strcmp(command, "clear") == 0)
  {
    clear_screen();
  }
  else if (strcmp(command, "mem") == 0)
  {
    mem(atoi(parameter));
  }
  else if (strcmp(command, "hstats") == 0)
  {
    printHeapStats();
  }
  else if (strcmp(command, "divOp") == 0)
  {
    invOp();
  }
  else if (strcmp(command, "div0") == 0)
  {
    div0();
  }
  else if (strcmp(command, "beep") == 0)
  {
    int flag = 1;
    for (int i = 0; parameter[i] != '\0' && flag; i++)
      flag = isdigit(parameter[i]);

    if (flag)
      beep(atoi(parameter));
    else
      perror("Invalid parameter\n");
  }

  else if (strcmp(command, "echo") == 0)
  {
    printf("%s", parameter);
  }

  else if (strcmp(command, "kill") == 0)
  {
    kill_process(atoi(parameter));
  }

  else if (strcmp(command, "nice") == 0)
  {
    nice(atoi(params[0]), atoi(params[1]));
  }

  else if (strcmp(command, "block") == 0)
  {
    block(atoi(parameter));
  }
  else if (strcmp(command, "sem") == 0)
  {
    printAllSemInfo();
  }
  else if (strcmp(command, "pipe") == 0)
  {
    printPipes();
  }
  else if (strcmp(command, "ps") == 0)
  {
    ps();
  }
  else if (strcmp(command, "width") == 0)
  {
    display_screen_width();
  }
  else if (strcmp(command, "height") == 0)
  {
    display_screen_height();
  }
  else if (strcmp(command, "time") == 0)
  {
    display_time();
  }
  else
  {
    perror("Unrecognized command.\n");
  }
}

static void display_time()
{
  int hour = system_hours();
  if (hour < 3)
  {
    hour = 24 - hour;
  }
  else
  {
    hour -= 3;
  }

  char hours[3];
  itoa(hour, 16, hours);
  if (strlen(hours) == 1)
  {
    hours[1] = hours[0];
    hours[0] = '0';
  }
  char minutes[3];
  itoa(system_minutes(), 16, minutes);
  if (strlen(minutes) == 1)
  {
    minutes[1] = minutes[0];
    minutes[0] = '0';
  }
  char seconds[3];
  itoa(system_seconds(), 16, seconds);
  if (strlen(seconds) == 1)
  {
    seconds[1] = seconds[0];
    seconds[0] = '0';
  }
  printf("%s : %s : %s\n", hours, minutes, seconds);
}

static void display_screen_width()
{
  printf("Screen width: %d\n", get_screen_width());
}

static void display_screen_height()
{
  printf("Screen height: %d\n", get_screen_height());
}

static void div0()
{
  clear_screen();
  _division_by_zero();
}

static void invOp()
{
  clear_screen();
  _invalid_op_code();
}

static void mem(uint64_t addr)
{

  printf("Printing from direction: %d\n", (int32_t)addr);
  printmem(addr);
  printf("\n");
}

static void parse_parameters(char *src, char *dest[])
{
  char *delim = " ";
  char *token;
  int k = 0;
  token = strtok(src, delim);
  while (token != NULL)
  {
    dest[k++] = token;
    token = strtok(NULL, delim);
  }
}
