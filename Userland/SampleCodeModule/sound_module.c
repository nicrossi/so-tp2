// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <sound_module.h>
#include <syscall.h>

void beep(int ticks) {
    _syscall(__beep, ticks);
}