// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pipe_module.h>

void putchar(char c)
{

  writeStd(&c, 1);
}
char getchar()
{
  char c;
  readStd(&c, 1);
  return c;
}
void perror(char *s)
{
  while (*s)
  {
    writePipe(s++, STD_ERR);
  }
}

void putint(int64_t value, int base)
{
  char number[20];
  int i = 0;
  int digits = 0;
  int negative = 0;
  if (value < 0)
  {
    value *= -1;
    negative = 1;
  }
  do
  {
    int remainder = value % base;
    number[i++] = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
    digits++;
  } while (value /= base);
  if (negative)
  {

    writeStd("-", 1);
  }
  for (i = digits - 1; i >= 0; i--)
    writeStd(&number[i], 1);
}

void printf(char *format, ...)
{
  va_list arg;
  va_start(arg, format);
  int i;
  char *s;
  for (; *format != 0; format++)
  {
    if (*format == '%')
    {
      format++;
      switch (*format)
      {
      case 'c':
        i = va_arg(arg, int);
        putchar(i);
        break;

      case 'd':
        i = va_arg(arg, int);
        putint(i, 10);
        break;

      case 's':
        s = va_arg(arg, char *);
        while (*s)
        {
          putchar(*s);
          s++;
        }
        break;
      }
    }
    else
    {
      putchar(*format);
    }
  }
  va_end(arg);
}

/*****************/
