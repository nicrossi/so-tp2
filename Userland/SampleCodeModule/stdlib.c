// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <stdlib.h>
#include <syscall.h>
#include <stdio.h>
#include <stdint.h>

void printHeapStats()
{
    HeapStats_t status;
    getHeapStats(&status);
    printf("\n#######################################################\n");
    printf("#                    HEAP STATS                       #");
    printf("\n#######################################################\n\n");
    printf("# Total heap space size: %db\n", (int32_t)status.totalHeapSizeInBytes);
    printf("# Ocuppied space size: %db\n", (int32_t)status.occupiedHeapSpaceInBytes);
    printf("# Available free space size: %db\n", (int32_t)status.availableHeapSpaceInBytes);
    printf("# Largest free block: %db\n", (int32_t)status.sizeOfLargestFreeBlockInBytes);
    printf("# Smallest free block: %db\n", (int32_t)status.sizeOfSmallestFreeBlockInBytes);
    printf("# Number of free blocks: %d\n", (int32_t)status.numberOfFreeBlocks);
    printf("# Number of succesful allocations: %d\n", (int32_t)status.numberOfSuccessfulAllocations);
    printf("# Number of succesful frees: %d\n\n", (int32_t)status.numberOfSuccessfulFrees);
}

void getHeapStats(HeapStats_t *status)
{
    _syscall(__heapStatus, status);
}

void *malloc(size_t size)
{

    return ((void *)((uint64_t)_syscall(__malloc, size)));
}

void free(void *ptr)
{
    _syscall(__free, ptr);
}

int strcmp(char *s1, char *s2)
{
    int i;
    for (i = 0; s1[i] != 0 || s2[i] != 0; i++)
    {
        if (s1[i] != s2[i])
            return 1;
    }
    return 0;
}

void strcpy(char *src, char *dest)
{
    int64_t length = strlen(src);
    for (int i = 0; i < length; i++)
    {
        dest[i] = src[i];
    }
}

int strlen(char *s)
{
    int i;
    for (i = 0; s[i] != '\0'; i++)
        ;
    return i;
}

char *itoa(int num, int base, char *str)
{
    int i = 0;
    int isNegative = 0;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = 1;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        num = num / base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    reverse(str, i);

    return str;
}

void reverse(char *str, int size)
{
    for (int i = 0; i < size / 2; i++)
    {
        char aux;
        aux = str[i];
        str[i] = str[size - i - 1];
        str[size - i - 1] = aux;
    }
}

int atoi(char *str)
{
    int res = 0; // Initialize result

    // Iterate through all characters of input string and
    // update result
    for (int i = 0; str[i] != '\0'; ++i)
        res = res * 10 + str[i] - '0';

    // return result.
    return res;
}

int isdigit(char c)
{
    return c >= '0' && c <= '9';
}

static int is_token(char c, char *token)
{
    while (*token != '\0')
    {
        if (c == *token)
            return 1;
        token++;
    }
    return 0;
}

char *strtok(char *s, char *token)
{
    static char *p; // start of the next search
    if (!s)
    {
        s = p;
    }
    if (!s)
    {
        // user is bad user
        return NULL;
    }

    // handle beginning of the string containing tokens
    while (1)
    {
        if (is_token(*s, token))
        {
            s++;
            continue;
        }
        if (*s == '\0')
        {
            return NULL; // reached the end of the string
        }

        break;
    }

    char *ret = s;
    while (1)
    {
        if (*s == '\0')
        {
            p = s; // next exec will return NULL
            return ret;
        }
        if (is_token(*s, token))
        {
            *s = '\0';
            p = s + 1;
            return ret;
        }
        s++;
    }
}

char *strcat(char *s1, const char *s2)
{
    char *ptr = s1 + strlen(s1);
    while (*s2 != '\0')
        *ptr++ = *s2++;

    *ptr = '\0';
    return s1;
}

uint32_t uintToBase(uint64_t value, char *buffer, uint32_t base)
{
    char *p = buffer;
    char *p1, *p2;
    uint32_t digits = 0;

    do
    {
        uint32_t remainder = value % base;
        *p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
        digits++;
    } while (value /= base);

    *p = 0;

    p1 = buffer;
    p2 = p - 1;

    while (p1 < p2)
    {
        char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        p1++;
        p2--;
    }
    return digits;
}