// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <stdio.h>
#include "test_util.h"

#include "test_processes.h"
enum State
{
  ERROR,
  RUNNING,
  BLOCKED,
  KILLED
};

typedef struct P_rq
{
  uint32_t pid;
  enum State state;
} p_rq;

//TO BE INCLUDED
void process_endless_loop()
{
  while (1)
    ;
  return;
}

void test_processes()
{
  p_rq p_rqs[MAX_PROCESSES];
  uint8_t rq;
  uint8_t alive = 0;
  uint8_t action;
  void (*entryPoint)(void);

  entryPoint = &process_endless_loop;

  while (1)
  {

    // Create MAX_PROCESSES processes
    for (rq = 0; rq < MAX_PROCESSES; rq++)
    {
      p_rqs[rq].pid = my_create_process("endless_loop", entryPoint); // TODO: Port this call as required

      if (p_rqs[rq].pid == -1)
      {                                     // TODO: Port this as required
        perror("Error creating process\n"); // TODO: Port this as required
        return;
      }
      else
      {
        p_rqs[rq].state = RUNNING;
        alive++;
      }
    }

    // // Randomly kills, blocks or unblocks processes until every one has been killed
    while (alive > 0)
    {

      for (rq = 0; rq < MAX_PROCESSES; rq++)
      {
        action = GetUniform(2) % 2;

        switch (action)
        {
        case 0:
          if (p_rqs[rq].state == RUNNING || p_rqs[rq].state == BLOCKED)
          {
            if (my_kill(p_rqs[rq].pid) == -1)
            { // TODO: Port this as required
              perror("Error killing process\n");

              return;
            }

            p_rqs[rq].state = KILLED;
            alive--;
          }
          break;

        case 1:
          if (p_rqs[rq].state == RUNNING)
          {
            if (my_block(p_rqs[rq].pid) == -1)
            {                                     // TODO: Port this as required
              perror("Error blocking process\n"); // TODO: Port this as required
              return;
            }

            p_rqs[rq].state = BLOCKED;
          }
          break;
        }
      }

      //Randomly unblocks processes
      for (rq = 0; rq < MAX_PROCESSES; rq++)
        if (p_rqs[rq].state == BLOCKED && GetUniform(2) % 2)
        {
          if (my_block(p_rqs[rq].pid) == -1)
          {                                       // TODO: Port this as required
            perror("Error unblocking process\n"); // TODO: Port this as required
            return;
          }

          p_rqs[rq].state = RUNNING;
        }
    }
  }
}
