// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <stdint.h>
#include <stdio.h>
#include <test_sync.h>

#include <sem_module.h>
#include <ps_module.h>

#define TOTAL_PAIR_PROCESSES 2
#define SEM_ID "test_sync"

int64_t global; //shared memory

static void dec_without_sem();
static void inc_without_sem();
static void dec();
static void inc();
static void slowInc(int64_t *p, int64_t inc);
static uint64_t my_sem_close(Sem_Info_t *sem);
static uint64_t my_sem_post(Sem_Info_t *sem);
static uint64_t my_sem_wait(Sem_Info_t *sem);
static Sem_Info_t *my_sem_open(char *name, uint64_t initialValue);

void test_no_sync()
{

  uint64_t i;

  global = 0;

  printf("CREATING PROCESSES...(WITHOUT SEM)\n");

  for (i = 0; i < TOTAL_PAIR_PROCESSES; i++)
  {
    void (*func)(void);
    func = &inc_without_sem;
    uint64_t p1[] = {13, 1, 0, 0};
    create_process("inc_without_sem", 0, 0, func, p1);
    //----------------------------------------

    func = &dec_without_sem;
    uint64_t p2[] = {13, 1, 0, 0};
    create_process("inc_without_sem", 0, 0, func, p2);
  }
}
void test_sync()
{
  uint64_t i;

  global = 0;

  printf("CREATING PROCESSES...(WITH SEM)\n");

  for (i = 0; i < TOTAL_PAIR_PROCESSES; i++)
  {
    void (*func)(void);
    func = &inc;

    uint64_t p1[] = {13, 1, 0, 0};
    create_process("inc", 0, 0, func, p1);
    //----------------------------------------
    func = &dec;
    uint64_t p2[] = {13, 1, 0, 0};
    create_process("dec", 0, 0, func, p2);
  }
}

static Sem_Info_t *my_sem_open(char *name, uint64_t initialValue)
{
  return semOpen(name, initialValue);
}

static uint64_t my_sem_wait(Sem_Info_t *sem)
{

  return semWait(sem);
}

static uint64_t my_sem_post(Sem_Info_t *sem)
{

  return semPost(sem);
}

static uint64_t my_sem_close(Sem_Info_t *sem)
{
  return semClose(sem);
}

static void slowInc(int64_t *p, int64_t inc)
{
  int64_t aux = *p;
  for (int i = 0; i < 100; i++)
  {
  }
  aux += inc;
  for (int i = 0; i < 100; i++)
  {
  }
  *p = aux;
}

static void inc()
{
  int64_t i;

  Sem_Info_t *sem = my_sem_open(SEM_ID, 1);

  if (sem == NULL)
  {
    perror("ERROR OPENING SEM\n");
    return;
  }

  for (i = 0; i < 100000; i++)
  {

    my_sem_wait(sem);

    slowInc(&global, 1);

    my_sem_post(sem);
  }

  my_sem_close(sem);

  printf("Final inc value: %d\n", (int32_t)global);
}

static void dec()
{
  int64_t i;

  Sem_Info_t *sem = my_sem_open(SEM_ID, 1);

  if (sem == NULL)
  {
    perror("ERROR OPENING SEM\n");
    return;
  }

  for (i = 0; i < 100000; i++)
  {
    my_sem_wait(sem);
    slowInc(&global, -1);

    my_sem_post(sem);
  }
  my_sem_close(sem);

  printf("Final dec value: %d\n", (int32_t)global);
}

static void inc_without_sem()
{
  int64_t i;
  for (i = 0; i < 1000000; i++)
  {
    slowInc(&global, 1);
  }

  printf("Final inc_no_sem value: %d\n", (int32_t)global);
}

static void dec_without_sem()
{
  int64_t i;
  for (i = 0; i < 1000000; i++)
  {
    slowInc(&global, -1);
  }
  printf("Final dec_no_sem value: %d\n", (int32_t)global);
}
