// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <syscall.h>

#include <stdint.h>
#include <video_module.h>

static int SCREEN_WIDTH = -1;
static int SCREEN_HEIGHT = -1;

void clear_screen()
{
	_syscall(__clear_screen);
}

int get_screen_width()
{
	return SCREEN_WIDTH = (SCREEN_WIDTH == -1) ? _syscall(__get_width) : SCREEN_WIDTH;
}

int get_screen_height()
{
	return SCREEN_HEIGHT = (SCREEN_HEIGHT == -1) ? _syscall(__get_height) : SCREEN_HEIGHT;
}
