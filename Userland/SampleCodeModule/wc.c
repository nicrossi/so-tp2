// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <wc.h>
#include <stdio.h>
#define ESC 27
void wc()
{
    int counter = 1;
    char c;
    while ((c = getchar()) != ESC)
    {
        putchar(c);
        if (c == '\n')
        {
            counter++;
        }
    }
    printf("Input lines quantity = %d\n", counter);
}